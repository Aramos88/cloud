﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  
 		CONFIDENTIAL
 		__________________
  
 		2016 - Antonio Nava Vargas 
 		All Rights Reserved.
  
 		NOTICE:  All information contained herein is, and remains
 		the property of Antonio Nava Vargas and its suppliers,
 		if any.  The intellectual and technical concepts contained
 		herein are proprietary to Antonio Nava Vargas
 		and its suppliers and may be covered by U.S. and Foreign Patents,
 		patents in process, and are protected by trade secret or copyright law.
 		Dissemination of this information or reproduction of this material
 		is strictly forbidden unless prior written permission is obtained
 		from Antonio Nava Vargas.
 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

using System.Data.Entity;
using System;
using Football.Models;

namespace Football.Context
{   /// </summary>
    public class FootballContext : DbContext
    {
        public FootballContext() : base("name=FootballApplicationDbContext")
        {
            //Local DataBase
            //AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
        }

        public static FootballContext Create()
        {
            return new FootballContext();
        }

        public override int SaveChanges()
        {
            // Throw if they try to call this
            throw new InvalidOperationException("This context is read-only.");
        }

        public virtual DbSet<Video> Videos { get; set; }
    }
}

﻿using System;

namespace AzureStreamingService
{
    public class StreamingServiceURI
    {
        public Uri ManifestAppleHLS { get; set; }
        public Uri ManifestMpegDash { get; set; }
        //public Uri Thumbnail { get; set; }
        public Uri ManifestSmoothStreaming { get; set; }
        public Uri MediaUrl { get; set; }
    }
}
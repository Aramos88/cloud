﻿using Microsoft.WindowsAzure.MediaServices.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AzureStreamingService
{
    public class StreamingService
    {
        public event EventHandler<StreamingServiceProgressHandler> UploadProgress = (s, e) => { };
        public event EventHandler<StreamingServiceFinishedHandler> UploadFinished = (s, e) => { };

        public event EventHandler<StreamingServiceProgressHandler> EncodingProgress = (s, e) => { };
        public event EventHandler<StreamingServiceFinishedHandler> EncodingFinished = (s, e) => { };

        public event EventHandler<StreamingServiceFinishedHandler> PublishingFinished = (s, e) => { };

        // Read values from the App.config file.
        private readonly string _mediaServicesAccountName;
        private readonly string _mediaServicesAccountKey;
        private string _extension;
        ///private IAsset _thumbnail;
        //
        // Field for service context.
        private CloudMediaContext context;
        private MediaServicesCredentials credentials;

        public StreamingService(string mediaServiceName, string mediaServiceAccount)
        {
            try
            {
                this._mediaServicesAccountName = mediaServiceName;
                this._mediaServicesAccountKey = mediaServiceAccount;
            }
            catch (Exception) { }
        }
        
        public StreamingServiceURI UploadVideo(string filePath)//path library or name file
        {
            try
            {
                // = filePath + ".mp4";
                // Create and cache the Media Services credentials & create CloudMediaContext
                this.credentials = new MediaServicesCredentials(this._mediaServicesAccountName, this._mediaServicesAccountKey);
                this.context = new CloudMediaContext(this.credentials);

                //var assetName = Guid.NewGuid() + "_" + DateTime.UtcNow.ToString() + "-" + filePath;
                var assetName = filePath;
                _extension = CatchExtension(filePath);
                // Add calls to methods defined in this section
                var inputAsset = UploadFile(assetName, AssetCreationOptions.None);
                var encodedAsset = EncodedBitrateMovil(inputAsset, AssetCreationOptions.None);
                var uris =  PublishAssetGetURLs(encodedAsset);
                return uris;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            
        }

        private IAsset EncodedBitrateMovil(IAsset asset, AssetCreationOptions options)
        {
            //IJob job = this.context.Jobs.CreateWithSingleTask(MediaProcessorNames.WindowsAzureMediaEncoder, 
            //    MediaEncoderTaskPresetStrings.H264AdaptiveBitrateMP4SetSD16x9,
            //    asset, "Bitrate "+asset.Name, options);
            var processor = context.MediaProcessors.GetLatestMediaProcessorByName(MediaProcessorNames.WindowsAzureMediaEncoder);

            IJob job = context.Jobs.CreateWithSingleTask(MediaProcessorNames.WindowsAzureMediaEncoder,
                MediaEncoderTaskPresetStrings.H264AdaptiveBitrateMP4SetSD16x9,
                //MediaEncoderTaskPresetStrings.H264AdaptiveBitrateMP4Set1080p,
                asset, "Bitrate " + asset.Name, options);
            
            //task.InputAssets.Add(asset);
            //var bloobThumbnail = task.OutputAssets.AddNew(string.Format("Thumbnail {0}", asset.Name), AssetCreationOptions.None);

            //Jobs.CreateWithSingleTask(MediaProcessorNames.WindowsAzureMediaPackager,
            //MediaEncoderTaskPresetStrings.H264AdaptiveBitrateMP4Set720p, 
            //asset, "Bitrate MP4 " + asset.Name, options);
            // Submit the job and wait until it is completed.
            job.Submit();
            
            var jobTask = job.GetExecutionProgressTask(CancellationToken.None);
            job = job.StartExecutionProgressTask
                  (
                    j =>
                    {
                        this.EncodingProgress(this, new StreamingServiceProgressHandler(j.GetOverallProgress()));
                    }, CancellationToken.None
                  ).Result;

            //Handler for finished Jobs
            if (job.State.Equals(JobState.Finished))
                this.EncodingFinished(this, new StreamingServiceFinishedHandler(true, false));

            if (job.State.Equals(JobState.Error))
                this.EncodingFinished(this, new StreamingServiceFinishedHandler(true, true));

            //_thumbnail = job.OutputMediaAssets[0];
            //PublicThumbnails(_thumbnail);
            return job.OutputMediaAssets[0];
        }

        //private Uri PublicThumbnails(IAsset thumbnail)
        //{
        //    //thumbnail
        //    //thumbnail.Locators .Create(LocatorType.Sas, thumbnail, AccessPermissions.Read, TimeSpan.FromDays(360 * 100));
        //    context.Locators.Create(LocatorType.None, thumbnail, AccessPermissions.Read, TimeSpan.FromDays(360 * 100));
        //    IEnumerable <IAssetFile> AssetFiles = thumbnail.AssetFiles.ToList().Where(af => af.Name.EndsWith(".jpg",StringComparison.Ordinal));
        //        //ToList().Where(af => af.Name.EndsWith(_extension, StringComparison.Ordinal));
        //    Uri thumbnailUri = thumbnail.Uri;

        //    PublishingFinished(context, new StreamingServiceFinishedHandler(true, false));
        //    return thumbnailUri;
        //}

        private IAsset UploadFile(string fileName, AssetCreationOptions options)
        {
            ////Upload asset & return the value
            return this.context.Assets.CreateFromFile(fileName, options, (af, p) =>
            {
                this.UploadProgress(this, new StreamingServiceProgressHandler(p.Progress));
            });
        }

        private void DeleteAsset(IAsset asset)
        {
            try
            {
                if (asset.Locators != null)
                    foreach (var locator in asset.Locators)
                        locator.Delete();

                asset.Delete();
            }
            catch (Exception) { }
        }

        private StreamingServiceURI PublishAssetGetURLs(IAsset asset)
        {
            StreamingServiceURI uri;
            // Publish the output asset by creating an Origin locator for adaptive streaming, and a SAS locator for progressive download.
            context.Locators.Create(LocatorType.OnDemandOrigin, asset, AccessPermissions.Read, TimeSpan.FromDays(360 * 100));
            context.Locators.Create(LocatorType.Sas, asset, AccessPermissions.Read, TimeSpan.FromDays(360 * 100));
            //context.Locators.Create(LocatorType.None, _thumbnail, AccessPermissions.Read, TimeSpan.FromDays(360 * 100));
            
            //var locator = context.Locators.CreateSasLocator(asset, context.AccessPolicies.Create("rad only", TimeSpan.FromDays(20),AccessPermissions.Read);

            IEnumerable<IAssetFile> VideoAssetFiles = asset.AssetFiles.
                ToList().Where(af => af.Name.EndsWith(_extension, StringComparison.Ordinal));
            

            // Get the Smooth Streaming, HLS and MPEG-DASH URLs for adaptive streaming and the Progressive Download URL.
            //uri = new StreamingServiceURI { ManifestSmoothStreaming = asset.GetSmoothStreamingUri(), ManifestAppleHLS = asset.GetHlsUri(), ManifestMpegDash = asset.GetMpegDashUri() };
            uri = new StreamingServiceURI {ManifestAppleHLS = asset.GetHlsUri(), ManifestMpegDash = asset.GetMpegDashUri()
                 };

            // Get the URls for progressive download for each MP4 file that was generated as a resultof encoding.
            List<Uri> mp4ProgressiveDownloadUris = VideoAssetFiles.Select(af => af.GetSasUri()).ToList();
                                                   //mp4AssetFiles.Select(af => af.GetSasUri()).ToList();
            mp4ProgressiveDownloadUris.ForEach(uria => uri.MediaUrl = uria);
            PublishingFinished(this, new StreamingServiceFinishedHandler(true, false));
            return uri;
        }

        private string CatchExtension(string path)
        {
            string extension;
            extension = Path.GetExtension(path);
            return extension;
        }

        public List<string> GetListOfFiles()
        {
            if (this.context != null)
                return this.context.Assets.Select(af => af.Name).ToList();

            return null;
        }
    }
}


﻿using System;

namespace AzureStreamingService
{
    public class StreamingServiceFinishedHandler : EventArgs
    {
        public Boolean Done { get; private set; }
        public Boolean Error { get; private set; }

        public StreamingServiceFinishedHandler(Boolean done, Boolean status) { this.Done = done; this.Error = status; }
    }

    public class StreamingServiceProgressHandler : EventArgs
    {
        public Double Progress { get; private set; }
        public StreamingServiceProgressHandler(Double progress) { this.Progress = progress; }
    }
}

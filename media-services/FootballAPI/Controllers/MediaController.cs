﻿using AzureStreamingService;
using Football.API.Helpers;
using Microsoft.WindowsAzure.MediaServices.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace Football.API.Controllers
{
    public class MediaController : ApiController
    {
        StreamingService streamingService;
        ServerHelpers helpers;
        public string mediaServicesName = ConfigurationManager.AppSettings.Get("MediaServicesAccountName");
        public string mediaServicesKey = ConfigurationManager.AppSettings.Get("MediaServicesAccountKey");

        [HttpPost]
        [Route("api/Video/Upload")]
        [ResponseType(typeof(StreamingServiceURI))]
        public IHttpActionResult Upload()
        {
            this.helpers = ServerHelpers.Instance;
            string tempFilePath = String.Empty;
            HttpRequest request;
            HttpPostedFile file;
            StreamingServiceURI uri;

            //Get file from HTTP post
            request = HttpContext.Current.Request;
            file = request.Files["media"];

            //File exists then save in servers temp storage
            if (file != null && file.ContentLength > 0)
            {
                //Guid guidName;
                //guidName = Guid.NewGuid();
                //var folderName = guidName + "_" + DateTime.UtcNow.ToString();
                
                tempFilePath = this.helpers.SaveFile(file, "~/UploadedMedia");
            }

            try
            {
                if (!string.IsNullOrEmpty(tempFilePath) && !string.IsNullOrWhiteSpace(tempFilePath))
                {
                    //New Azure streaming services
                    this.streamingService = new StreamingService(mediaServicesName, mediaServicesKey);

                    //Event Handlers

                    //Upload from server, Encode & Publish Media Files
                    uri = this.streamingService.UploadVideo(tempFilePath);

                    //Remove temporal File
                    this.helpers.RemoveFile(tempFilePath);

                    return Ok(uri);
                }
                else
                    throw new Exception("Empty or null mediaPath");
            }
            catch (Exception e)
            {
                Trace.WriteLine("Football.API.AzureStreamingService " + e.Message);
                return InternalServerError();
            }
        }

        [Route("api/Audio")]
        [ResponseType(typeof(List<string>))]
        public IHttpActionResult Get()
        {
            List<string> listNames;
            if(this.streamingService == null)
                this.streamingService = new StreamingService(mediaServicesName, mediaServicesKey);

            //Get all files from AssetS
            listNames = this.streamingService.GetListOfFiles();

            if (listNames != null)
                return Ok(this.streamingService.GetListOfFiles());
            else
                return NotFound();
        }

        // PUT: api/Upload/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Upload/5
        public void Delete(int id)
        {
        }

        private void EventUploadProgress(Object sender, StreamingServiceProgressHandler e)
        {
            Trace.WriteLine("Upload progress " + e.Progress);
        }

        private void EventEncodingProgress(Object sender, StreamingServiceProgressHandler e)
        {
            Trace.WriteLine("Encoder progress " + e.Progress);
        }

        private void EventEncodingFinished(Object sender, StreamingServiceFinishedHandler e)
        {
            Trace.WriteLine("Encoding finished " + e.Done);
            Trace.WriteLine("Encoding error " + e.Error);
        }

        private void EventPublishingFinished(Object sender, StreamingServiceFinishedHandler e)
        {
            Trace.WriteLine("Publishing finished " + e.Done);
            Trace.WriteLine("Publishing error " + e.Error);
        }

        //public void Dispose()
        //{
        //    service.UploadProgress -= EventUploadProgress;
        //    service.EncodingProgress -= EventEncodingProgress;
        //    service.EncodingFinished -= EventEncodingFinished;
        //    service.PublishingFinished -= EventPublishingFinished;
        //}
    }
}

﻿using System.IO;
using System.Web;

namespace Football.API.Helpers
{
    public class ServerHelpers
    {
        //Singleton
        private static ServerHelpers instance;
        private HttpServerUtility server = HttpContext.Current.Server;

        private ServerHelpers()
        { }

        //Singleton
        public static ServerHelpers Instance
        {
            get
            {
                if (instance == null)
                    instance = new ServerHelpers();
                return instance;
            }
        }

        public string SaveFile(HttpPostedFile file, string path)
        {
            string serverStorage;
            string uploadedFileName;

            serverStorage = this.server.MapPath(path);
            uploadedFileName = file.FileName;
            string changeName = uploadedFileName + ".mp4";

            if (!Directory.Exists(serverStorage))
                Directory.CreateDirectory(serverStorage);

            file.SaveAs(string.Format(@"{1}\{0}", changeName, serverStorage));

            return string.Format(@"{1}\{0}", changeName, serverStorage);
        }

        public void RemoveFile(string filePath)
        {
            File.Delete(filePath);
        }
    }
}
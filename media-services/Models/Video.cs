﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Football.Models
{
    //[Table("videos")]
    public class Video
    {
        [Key]
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [Required]
        [MaxLength(100)]
        [JsonProperty(PropertyName = "user_id")]
        public string IdUser { get; set; }

        [MaxLength(255)]
        [JsonProperty(PropertyName = "video_url")]
        public string Url { get; set; }
    }
}

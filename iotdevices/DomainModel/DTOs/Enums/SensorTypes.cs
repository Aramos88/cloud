﻿
namespace DomainModel.DTOs.Enums
{
    public enum SensorTypes
    {
        Temperature = 1,
        Ph,
        Ambient_Light_Intensity,
        Fluid_Volume,
        Electric_Current,
        Color,
        Distance,
        Force,
        Humidity,
        XZ_Plane_Distance,
        Volumetric_Flow_Rate,
        Presence
    };
}
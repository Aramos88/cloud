﻿using DomainModel.Entities.Business;
using DomainModel.Entities.Enums;

namespace ServiceApi.Models
{
    public class UpdateDeviceBindingModel
    {
        public string ETag { get; set; }

        public string StatusReason { get; set; }

        public DeviceState CurrentState { get; set; }

        public SensorType Sensor { get; set; }

        public string ModelNumber { get; set; }

        public string SerialNumber { get; set; }

        public string SymmetricPrimaryKey { get; set; }

        public string SymmetricSecondaryKey { get; set; }
    }
}
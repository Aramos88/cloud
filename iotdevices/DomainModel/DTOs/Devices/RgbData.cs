﻿
namespace DomainModel.DTOs.Devices
{
    public class RgbData
    {
        public RgbMeasure Measure { get; set; }

        public string Units { get; set; }
    }

    public class RgbMeasure
    {
        public int r { get; set; }

        public int g { get; set; }

        public int b { get; set; }
    }
}
﻿namespace DomainModel.DTOs.Devices
{
    public class XZDistanceMeasurement
    {
        public int DeviceInfoId { get; set; }

        public string SentAt { get; set; }

        public XZDistanceData Data { get; set; }

        public int Count { get; set; }

        public bool Trigger { get; set; }
    }
}
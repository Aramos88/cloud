﻿
namespace DomainModel.DTOs.Devices
{
    public class XZDistanceData
    {
        public XZMeasure Measure { get; set; }

        public string Units { get; set; }
    }

    public class XZMeasure
    {
        public double x { get; set; }

        public double z { get; set; }
    }
}
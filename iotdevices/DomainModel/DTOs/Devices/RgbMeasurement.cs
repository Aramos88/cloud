﻿namespace DomainModel.DTOs.Devices
{
    public class RgbMeasurement
    {
        public int DeviceInfoId { get; set; }

        public string SentAt { get; set; }

        public RgbData Data { get; set; }

        public int Count { get; set; }

        public bool Trigger { get; set; }
    }
}
﻿namespace DomainModel.DTOs.Devices
{
    public class GatewayMeasurement
    {
        public int DeviceInfoId { get; set; }

        public double Measure { get; set; }

        public string Unit { get; set; }
    }
}
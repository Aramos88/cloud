﻿using System;
using Newtonsoft.Json;

namespace DomainModel.DTOs.Devices
{
    public class DevicesData
    {
        
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "deviceinfoid")]
        public int ApplicationDeviceId { get; set; }

        [JsonProperty(PropertyName = "eventprocessedutctime")]
        public string EventProcessedUtcTime { get; set; }

        [JsonProperty(PropertyName = "eventenqueuedutctime")]
        public string EventEnqueuedUtcTime { get; set; }

        [JsonProperty(PropertyName = "sentat")]
        public DateTime SentAt { get; set; }

        [JsonProperty(PropertyName = "data")]
        public object Data { get; set; }

        [JsonProperty(PropertyName = "receivedat")]
        public DateTime ReceivedAt { get; set; }

        //[JsonProperty(PropertyName = "_rid")]
        //public string Rid { get; set; }

        //[JsonProperty(PropertyName = "_self")]
        //public string Self { get; set; }

        //[JsonProperty(PropertyName = "_etag")]
        //public string Etag { get; set; }

        //[JsonProperty(PropertyName = "_attachments")]
        //public string Attachments { get; set; }

        //[JsonProperty(PropertyName = "_ts")]
        //public long Ts { get; set; }
    }
    
}
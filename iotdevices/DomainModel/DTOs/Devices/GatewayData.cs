﻿
namespace DomainModel.DTOs.Devices
{
    public class GatewayData
    {
        public double Measure { get; set; }

        public string Units { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace DomainModel.DTOs.Devices
{
    public class RegisterSensorBindingModel
    {
        [Required]
        [StringLength(40, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string Name { get; set; }
    }
}
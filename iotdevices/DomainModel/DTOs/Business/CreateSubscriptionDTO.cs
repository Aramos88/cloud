﻿using DomainModel.Entities.Enums;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DomainModel.DTOs.Business
{
    public class CreateSubscriptionDTO
    {
        [Required]
        [EmailAddress]
        [JsonProperty("email")]
        public string ContactMail { get; set; }

        [Required]
        [JsonProperty("subscription_plan")]
        public SubscriptionPlan SubscriptionPlan { get; set; }

        [Required]
        [JsonProperty("device_codes")]
        public List<string> DeviceCodes { get; set; }
    }
}

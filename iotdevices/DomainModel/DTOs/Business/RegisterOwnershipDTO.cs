﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DomainModel.DTOs.Business
{
    public class RegisterOwnershipDTO
    {
        [Required]
        [JsonProperty("device_codes")]
        public ICollection<string> DeviceCodes { get; set; }
    }
}

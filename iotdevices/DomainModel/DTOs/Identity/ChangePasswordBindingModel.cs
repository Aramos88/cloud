﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace DomainModel.DTOs.Identity
{
    public class ChangePasswordBindingModel
    {
        [Required]
        [DataType(DataType.Password)]
        [JsonProperty("current_password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [JsonProperty("new_password")]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [JsonProperty("confirm_password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
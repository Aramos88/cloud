﻿using DomainModel.Models.Identity;
using System.Collections.Generic;

namespace DomainModel.DTOs.Identity
{
    public class UserRolesBindingModel
    {
        public string RoleId { get; set; }
        public ICollection<UserProfile> EnrolledUsers { get; set; }
        public ICollection<UserProfile> RevokedUsers { get; set; }
    }
}
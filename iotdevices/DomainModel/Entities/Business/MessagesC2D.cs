﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace DomainModel.Entities.Business
{
    public class MessagesC2D
    {
        [Key]
        [JsonIgnore]
        public Guid Id { get; set; }

        [MaxLength(100)]
        [JsonProperty("user")]
        [DataMember(EmitDefaultValue = false)]
        public string UserSending { get; set; }

        [JsonProperty("msg")]
        [DataMember(EmitDefaultValue = false)]
        public MsgBody BodyMessage { get; set; }

        [MaxLength(100)]
        [JsonProperty("deviceName")]
        [DataMember(EmitDefaultValue = false)]
        public string DeviceName { get; set; }

        private DateTime? _createdAt;

        [JsonIgnore]
        public DateTime CreatedAt
        {
            get { return _createdAt ?? DateTime.Now; }
            set { _createdAt = value; }
        }

        [JsonIgnore]
        [DataMember(EmitDefaultValue = false)]
        public bool ResponseFeedBack { get; set; }
    }

    public class MsgBody
    {
        [MaxLength(100)]
        [JsonProperty("deviceIp")]
        public string DeviceIp { get; set; }

        [MaxLength(255)]
        [JsonProperty("msg")]
        public string Msg { get; set; }

    }

}

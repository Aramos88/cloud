﻿using System;

namespace DomainModel.Entities.Business
{
    public class DeviceMeasurement
    {
        public int Id { get; set; }

        public DateTime SentAt { get; set; }

        public DateTime ReceivedAt { get; set; }

        public DateTime EventProcessedUtcTime { get; set; }

        public DateTime EventEnqueuedUtcTime { get; set; }

        public string Data { get; set; }

        public int ApplicationDeviceId { get; set; }
        public virtual ApplicationDevice ApplicationDevice { get; set; }
    }
}

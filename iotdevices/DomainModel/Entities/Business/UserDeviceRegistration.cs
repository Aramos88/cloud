﻿using DomainModel.Entities.Identity;
using System;

namespace DomainModel.Entities.Business
{
    public class UserDeviceRegistration
    {
        public int Id { get; set; }

        public bool IsCompleted { get; set; }

        public string ConfirmationToken { get; set; }

        public DateTime CompletedOn { get; set; }

        public string DeviceCodes { get; set; }

        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}

﻿namespace DomainModel.Entities.Business
{
    public class ContactInfo
    {
        public string Email { get; set; }

        public string Address { get; set; }

        public string HomePhone { get; set; }

        public string MobilePhone { get; set; }
    }
}

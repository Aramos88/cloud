﻿using DomainModel.Entities.Enums;
using System;
using System.Collections.Generic;

namespace DomainModel.Entities.Business
{
    public class Subscription
    {
        public int Id { get; set; }

        public string ContactMail { get; set; }

        public int NumberOfDevices { get; set; }

        public SubscriptionPlan PlanType { get; set; }

        public DateTime BeginningDate { get; set; }

        public DateTime DueDate { get; set; }

        public virtual ICollection<Invoice> Invoices { get; set; }

        public virtual ICollection<ApplicationDevice> Devices { get; set; }
    }
}

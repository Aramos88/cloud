﻿using System;
using System.Collections.Generic;
using DomainModel.Entities.Enums;
using DomainModel.Entities.Identity;
using Microsoft.Azure.Devices;

namespace DomainModel.Entities.Business
{
    public class ApplicationDevice
    {
        public ApplicationDevice()
        {
            Measurements = new HashSet<DeviceMeasurement>();
        }

        public int Id { get; set; }

        public string Description { get; set; }

        public string Code { get; set; }

        public string Key { get; set; }

        public string DeviceName { get; set; }

        public bool LeasedOrSold { get; set; }

        public string ModelNumber { get; set; }

        public string SerialNumber { get; set; }

        public bool SimulationMode { get; set; }

        public DeviceState CurrentState { get; set; }

        public DeviceConnectionState ConnectionState { get; }

        public DeviceStatus Status { get; set; }

        public int CloudToDeviceMessageCount { get; set; }

        public DateTime ConnectionStateUpdatedTime { get; set; }

        public DateTime LastActivityTime { get; set; }

        public int SensorTypeId { get; set; }
        public virtual SensorType SensorType { get; set; }

        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public int? SubscriptionId { get; set; }
        public virtual Subscription Subscription { get; set; }

        public virtual ICollection<DeviceMeasurement> Measurements { get; set; }

        public virtual ICollection<C2DArduinoMessage> C2DArduinoMessages { get; set; }
    }
}

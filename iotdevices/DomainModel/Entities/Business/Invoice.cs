﻿namespace DomainModel.Entities.Business
{
    public class Invoice
    {
        public int Id { get; set; }

        public decimal TotalDue { get; set; }

        public decimal TotalPayed { get; set; }

        public decimal Tax { get; set; }

        public decimal ServiceFee { get; set; }

        public ContactInfo ContactInfo { get; set; }

        public int SubscriptionId { get; set; }
        public virtual Subscription Subscription { get; set; }
    }
}

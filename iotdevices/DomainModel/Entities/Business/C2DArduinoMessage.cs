﻿using System;
using System.ComponentModel.DataAnnotations;
using DomainModel.Entities.Identity;
using Newtonsoft.Json;

namespace DomainModel.Entities.Business
{
    public class C2DArduinoMessage
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonIgnore]
        public string ApplicationUserId { get; set; }
        [JsonIgnore]
        public ApplicationUser ApplicationUser { get; set; }

        [Required]
        [JsonProperty("application_device_id")]
        public int ApplicationDeviceId { get; set; }
        [JsonIgnore]
        public ApplicationDevice ApplicationDevice { get; set; }

        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("parameters")]
        public string Parameters { get; set; }

        [JsonProperty("created_at")]
        public DateTime? CreatedAt { get; set; } = DateTime.Now;
        
        [JsonIgnore]
        [JsonProperty("response_feedback")]
        public bool ResponseFeedBack { get; set; }

        //C2D Required Parameters
        public override string ToString()
        {
            return JsonConvert.SerializeObject(new
            {
                Id = Guid.NewGuid(),
                Name,
                Parameters
            });
        }
    }
}

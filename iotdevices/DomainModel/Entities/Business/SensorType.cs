﻿namespace DomainModel.Entities.Business
{
    public class SensorType
    {
        public int Id { get; set; }

        public string Value { get; set; }
    }
}

﻿namespace DomainModel.Entities.Business
{
    public class DeviceDiagnostic
    {
        public int Id { get; set; }

        public string BatteryLife { get; set; }

        public string StatusConnection { get; set; }

        public string Temperature { get; set; }

        public int ApplicationDeviceId { get; set; }
        public virtual ApplicationDevice ApplicationDevice { get; set; }
    }
}

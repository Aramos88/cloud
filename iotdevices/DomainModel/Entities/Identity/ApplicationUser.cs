﻿using DomainModel.Entities.Business;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DomainModel.Entities.Identity
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            Devices = new HashSet<ApplicationDevice>();
            DeviceRegistrations = new HashSet<UserDeviceRegistration>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime JoinedOn { get; set; }

        public virtual Token Token { get; set; }
        public virtual ICollection<ApplicationDevice> Devices { get; set; }
        public virtual ICollection<UserDeviceRegistration> DeviceRegistrations { get; set; }
        public virtual ICollection<C2DArduinoMessage> C2DMessages { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
}

﻿namespace DomainModel.Entities.Enums
{
    public enum DeviceState
    {
        NotReachable,
        Functioning,
        CriticalFunctioning,
        Damaged,
        NeedsRepair
    };
}

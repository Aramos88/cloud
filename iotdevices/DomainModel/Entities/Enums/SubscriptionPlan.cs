﻿namespace DomainModel.Entities.Enums
{
    public enum SubscriptionPlan
    {
        Home,
        Enterprise
    };
}

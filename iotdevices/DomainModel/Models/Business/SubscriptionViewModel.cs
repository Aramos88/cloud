﻿using DomainModel.Entities.Enums;
using DomainModel.Models.Devices;
using System;
using System.Collections.Generic;

namespace DomainModel.Models.Business
{
    public class SubscriptionViewModel
    {
        public int Id { get; set; }

        public int NumberOfDevices { get; set; }

        public SubscriptionPlan PlanType { get; set; }

        public DateTime BeginningDate { get; set; }

        public DateTime DueDate { get; set; }

        public virtual IEnumerable<ApplicationDeviceViewModel> Devices { get; set; }
    }
}

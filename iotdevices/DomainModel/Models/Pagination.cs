﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DomainModel.Models
{
    public class Pagination<T>
    {
        //public ICollection<Page<T>> Pages { get; set; }
        public int TotalPages { get; private set; }

        private double _resultsPerPage;
        public double ResultsPerPage
        {
            get { return _resultsPerPage; }
            set { _resultsPerPage = value < 1 ? 1 : value; }
        }

        private double _pageRequested;
        private int pageSize;
        private int page;
        private readonly string _absolutePath;

        public double PageRequested
        {
            get { return _pageRequested; }  
            set { _pageRequested = value < 1 ? 1 : value; }
        }

        public Page<T> ActualPage { get; set; }
        public Page<T> Next { get; set; } 
        public Page<T> Prev { get; set; }

        public Pagination(): this(10, 1)
        {
        }

        public Pagination(double resultsPerPage, double pageRequested)
        {
            ResultsPerPage = resultsPerPage;
            PageRequested = pageRequested;
            _absolutePath = HttpContext.Current.Request.Url.AbsolutePath;
        }

        public void CreatePage(List<T> rows)
        {
            throw new NotImplementedException();
        }

        public void CreatePages(List<T> rows)
        {
            throw new NotImplementedException();
        }

        public void QueryBuilder<TQuery>(IQueryable<TQuery> query, Func<IQueryable<TQuery>, IQueryable<T>> querySelect = null)
        {
            TotalPages = (int)Math.Ceiling((query.Count()) / ResultsPerPage);

            IQueryable<T> queryResult;

            if (querySelect != null)
                queryResult = querySelect(query.Skip(GetSkip()).Take(GetTake()));
            else
                queryResult = (IQueryable<T>)query.Skip(GetSkip()).Take(GetTake());

            var result = queryResult.ToList();

            if ((int)PageRequested > TotalPages)
                throw new Exception("Page not exists");

            if (TotalPages == 1 && (int)PageRequested == 1)
            {
                Next = Prev = null;
                ActualPage = new Page<T>(result.Take((int)ResultsPerPage), _absolutePath + $"?page={PageRequested}&pageSize={ResultsPerPage}");
            }
            else if (TotalPages > 1 && (int)PageRequested == 1) //First Page
            {
                Prev = null;
                ActualPage = new Page<T>(result.Skip((int)ResultsPerPage).Take((int)ResultsPerPage), _absolutePath + $"?page={PageRequested}&pageSize={ResultsPerPage}");
                Next = new Page<T>(result.Skip((int)ResultsPerPage).Take((int)ResultsPerPage), _absolutePath + $"?page={PageRequested + 1}&pageSize={ResultsPerPage}");
            }
            else if ((int)PageRequested == TotalPages) //Last Page
            {
                Prev = new Page<T>(result.Take((int)ResultsPerPage),
                    _absolutePath + $"?page={PageRequested - 1}&pageSize={ResultsPerPage}");
                ActualPage = new Page<T>(result.Skip((int)ResultsPerPage).Take((int)ResultsPerPage),
                    _absolutePath + $"?page={PageRequested}&pageSize={ResultsPerPage}");
                Next = null;
            }
            else
            {
                Prev = new Page<T>(result.Take((int)ResultsPerPage), _absolutePath + $"?page={PageRequested - 1}&pageSize={ResultsPerPage}");
                ActualPage = new Page<T>(result.Skip((int)ResultsPerPage).Take((int)ResultsPerPage), _absolutePath + $"?page={PageRequested}&pageSize={ResultsPerPage}");
                Next = new Page<T>(result.Skip((int)ResultsPerPage * 2).Take((int)ResultsPerPage), _absolutePath + $"?page={PageRequested + 1}&pageSize={ResultsPerPage}");
            }

        }

        private int GetSkip() => (int)(ResultsPerPage * ((PageRequested - 2) > 0 ? (PageRequested - 2) : 0));

        private int GetTake() => (int)(ResultsPerPage * 3);

    }
}

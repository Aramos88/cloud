﻿using System.Collections.Generic;
using System.Linq;

namespace DomainModel.Models
{
    public class Page<T>
    {
        private string _absolutePath;

        public IEnumerable<T> Data { get; set; }
        public string Uri { get; set; } 
        public int ItemsCount
        {
            get { return Data.Count(); } 
        }
        public string DataType { get; set; }

        public Page(IEnumerable<T> data)
        {
            Data = data;
            DataType = string.Empty;
        }

        public Page(IEnumerable<T> data, string absolutePath)
        {
            Data = data;
            Uri = absolutePath;
            DataType = typeof(T).FullName;
        }
    }
}

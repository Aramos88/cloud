﻿using DomainModel.Entities.Enums;
using Microsoft.Azure.Devices;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace DomainModel.Models.Devices
{
    public class UpdateDeviceModel
    {
        [Required]
        public string Name { get; set; }

        #region CloudUpdateableOnly

        public string ETag { get; set; }

        [JsonProperty("status_reason")]
        public string StatusReason { get; set; }

        public DeviceStatus Status { get; set; }

        #endregion
        [JsonProperty("simulation_mode")]
        public bool SimulationMode { get; set; }

        [JsonProperty("current_state")]
        public DeviceState CurrentState { get; set; }

        [JsonProperty("sensor_id")]
        public int SensorId { get; set; }
    }
}
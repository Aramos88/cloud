﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace DomainModel.Models.Devices
{
    public class CreateDeviceModel
    {
        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }

        [Required]
        [JsonProperty("description")]
        public string Description { get; set; }

        [Required]
        [JsonProperty("model_number")]
        public string ModelNumber { get; set; }

        [Required]
        [JsonProperty("serial_number")]
        public string SerialNumber { get; set; }

        [Required]
        [JsonProperty("sensor_type_id")]
        public int SensorTypeId { get; set; }

        [JsonProperty("simulation_mode")]
        public bool SimulationMode { get; set; }
    }
}
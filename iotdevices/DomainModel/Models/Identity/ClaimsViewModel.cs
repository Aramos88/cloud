﻿
namespace DomainModel.Models.Identity
{
    public class ClaimsViewModel
    {
        public string Type { get; set; }

        public string Value { get; set; }

        public string ValueType { get; set; }
    }
}
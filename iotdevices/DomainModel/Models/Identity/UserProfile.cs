﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DomainModel.Models.Identity
{
    public class UserProfile
    {
        public string Url { get; set; }
        public string Id { get; set; }

        [JsonProperty("username")]
        public string UserName { get; set; }

        [JsonProperty("full_name")]
        public string FullName { get; set; }

        public string Email { get; set; }

        [JsonProperty("email_confirmed")]
        public bool EmailConfirmed { get; set; }

        [JsonProperty("join_date")]
        public DateTime JoinDate { get; set; }

        public IEnumerable<string> Roles { get; set; }
        public IEnumerable<ClaimsViewModel> Claims { get; set; }
    }
}
﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace DomainModel.Models.Identity
{
    public class AuthTokenReturnModel
    {
        [Required]
        [JsonProperty("user_name")]
        public string Username { get; set; }

        [Required]
        [JsonProperty("email")]
        public string Email { get; set; }

        [Required]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [Required]
        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [Required]
        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }
    }
}
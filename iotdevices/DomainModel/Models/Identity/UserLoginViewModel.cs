﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace DomainModel.Models.Identity
{
    public class UserLoginViewModel
    {
        [Required]
        [JsonProperty("username")]
        public string Username { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
﻿namespace DomainModel.Models.Identity
{
    public class RoleViewModel
    {
        public string Id { get; set; }

        public string Url { get; set; }

        public string Name { get; set; }
    }
}
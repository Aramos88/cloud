﻿using System;

namespace ServiceApi.Swag.Attributes
{
    public class SwaggerMultipleResponseTypesAttribute : Attribute
    {
        public Type ResponseType { get; set; }
        public Type ExamplesType { get; set; }

        public SwaggerMultipleResponseTypesAttribute(Type responseType,
            Type examplesType)
        {
            ResponseType = responseType;
            ExamplesType = examplesType;
        }
    }
}
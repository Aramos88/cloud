﻿using DomainModel.DTOs.Devices;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace ServiceApi.Swag.Attributes
{
    public class DeviceMeasurementsExamples
    {
        private const string DateTimeFormatString = "yyyy-MM-dd HH:mm:ss";

        public static object GetExamples()
        {
            return new List<JObject>
            {
                JObject.FromObject(new GatewayMeasurement
                {
                    DeviceInfoId = 1,
                    Measure = 1,
                    Unit = "any_unit"
                }),
                JObject.FromObject(new XZDistanceMeasurement
                {
                    DeviceInfoId = 1,
                    SentAt = DateTime.UtcNow.ToLocalTime().ToString(DateTimeFormatString),
                    Data = new XZDistanceData
                    {
                        Measure = new XZMeasure
                        {
                            x = 1,
                            z = 1
                        },
                        Units = "cm_m"
                    }
                }),
                JObject.FromObject(new RgbMeasurement
                {
                    DeviceInfoId = 1,
                    SentAt = DateTime.UtcNow.ToLocalTime().ToString(DateTimeFormatString),
                    Data = new RgbData
                    {
                        Measure = new RgbMeasure
                        {
                            r = 1,
                            g = 2,
                            b = 3
                        },
                        Units = "RGB"
                    }
                })
            };
        }
    }
}
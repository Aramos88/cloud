﻿using ServiceApi.Swag.Attributes;
using Swashbuckle.Swagger;
using System.Linq;
using System.Web.Http.Description;

namespace ServiceApi.Swag.OperationFilters
{
    public class SwaggerMultipleResponseTypesOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            var responseAttributes = apiDescription.GetControllerAndActionAttributes<SwaggerMultipleResponseTypesAttribute>();

            foreach (var attr in responseAttributes)
            {
                var attrSchema = schemaRegistry.GetOrRegister(attr.ResponseType);

                var response = operation.responses.FirstOrDefault(r => r.Value.schema.type == attrSchema.type
                && r.Value.schema.@ref == attrSchema.@ref).Value;

                if (response != null)
                {
                    response.examples = DeviceMeasurementsExamples.GetExamples();
                }
            }
        }
    }
}
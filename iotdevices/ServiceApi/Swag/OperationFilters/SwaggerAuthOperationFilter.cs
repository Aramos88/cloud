﻿using ServiceApi.Jwt;
using Swashbuckle.Swagger;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace ServiceApi.Swag.OperationFilters
{
    public class SwaggerAuthOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
       {
#if DEBUG
            var jwtAuthorized = apiDescription.ActionDescriptor.GetFilterPipeline()
                                                                                .Select(fi => fi.Instance)
                                                                                .Any(f => f is JwtAuthorize);

            if (jwtAuthorized)
            {
                if (apiDescription.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Count > 0)
                {
                    return;
                }

                operation.parameters = operation.parameters ?? new List<Parameter>();

                operation.parameters.Add(new Parameter
                {
                    name = "Authorization",
                    @in = "header",
                    description = "Bearer",
                    required = true,
                    type = "string"
                });
            }
#endif
        }

    }
}
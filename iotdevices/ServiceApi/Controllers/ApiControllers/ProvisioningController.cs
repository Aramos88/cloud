﻿using System;
using System.Web;
using System.Linq;
using Microsoft.Azure.Devices;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Http;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using Microsoft.AspNet.Identity;
using System.Net;
using System.Text;
using Microsoft.Azure.Devices.Common.Exceptions;
using Microsoft.WindowsAzure.Storage.Blob;
using Swashbuckle.Swagger.Annotations;
using ServiceApi.Jwt;
using ServiceApi.Libraries;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Security.Claims;
using Newtonsoft.Json;
using DomainModel.Entities.Business;
using DomainModel.Entities.Enums;
using DomainModel.Models.Devices;

namespace ServiceApi.Controllers.ApiControllers
{
    [JwtAuthorize(Roles = "SuperAdmin, Admin")]
    [RoutePrefix("api/provisioning")]
    public class ProvisioningController : BaseApiController
    {
        private const string IoTMaxDevicesKey = "as:IoTMaxDevices";
        private const string IoTConnectionStringKey = "as:IoTConnectionString";

        private readonly string MaximumNumberOfDevicesMessage;
        private readonly string DeviceIdCannotBeNull;
        private readonly string SuccessfullyDeleted;
        private RegistryManager DeviceRegistryManager;

        

        public ProvisioningController()
            : base()
        {
            MaximumNumberOfDevicesMessage = "You have reached the maximum number of devices";
            DeviceIdCannotBeNull = "Failed to register device. DeviceId cannot be null or whitespace.";
            SuccessfullyDeleted = "Succefully deleted from registry device {0}";
            DeviceRegistryManager = RegistryManager.CreateFromConnectionString(
                     ConfigurationManager.AppSettings[IoTConnectionStringKey]);
        }

        [SwaggerResponse(HttpStatusCode.OK, "Successfully fetched all devices registered in the IoT Hub", typeof(IEnumerable<Device>))]
        [SwaggerResponse(HttpStatusCode.Conflict, "Connection failed when trying to get devices from IoTHub")]
        [Route("getDevices")]
        public async Task<IHttpActionResult> Get()
        {
            return await GetDevices();
        }

        [HttpPost]
        [SwaggerResponse(HttpStatusCode.Created, "Successfully registered a new device both to Sql Azure storage and IoT Hub", typeof(ApplicationDevice))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Either model is not valid or the user has exceeded the maximum number of devices to be registered.")]
        [SwaggerResponse(HttpStatusCode.Conflict, "The device already exists.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "A problem occurred when trying to register the device to cloud components.")]
        [Route("create")]
        public async Task<IHttpActionResult> Post([FromBody] CreateDeviceModel model)
        {
            if (!ModelState.IsValid ||
                model == null)
            {
                return BadRequest(ModelState);
            }

            ApplicationDevice device = null;
            string userId = HttpContext.Current.User.Identity.GetUserId();
            var user = DbContext.Users.Include(au => au.Devices).FirstOrDefault(au => au.Id == userId);

            if (user.Devices.Count >= int.Parse(ConfigurationManager.AppSettings[IoTMaxDevicesKey]))
            {
                return BadRequest(MaximumNumberOfDevicesMessage);
            }

            try
            {
                var result = await DeviceRegistryManager.AddDeviceAsync(new Device(PrepareIdStringToCode(model.Name))
                {
                    Status = DeviceStatus.Enabled
                });

                device = new ApplicationDevice
                {
                    DeviceName = model.Name,
                    Description = model.Description,
                    ModelNumber = model.ModelNumber,
                    SerialNumber = model.SerialNumber,
                    SensorTypeId = model.SensorTypeId,
                    Code = result.Id,
                    Key = result.Authentication.SymmetricKey.PrimaryKey,
                    CurrentState = DeviceState.Functioning,
                    Status = DeviceStatus.Enabled
                };

                user.Devices.Add(device);
                DbContext.Users.AddOrUpdate(user);
                if (DbContext.SaveChanges() > 0)
                {                   
                    CreateUpdateDeviceBlob();
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }

                return InternalServerError(dbEx);
            }
            catch (DeviceAlreadyExistsException ex)
            {
                return Content(HttpStatusCode.Conflict, ex.Message);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }

            return Created("api/provisioning", device);
        }

        [JwtAuthorize]
        [Route("bulk")]
        public async Task<IHttpActionResult> BulkInsert(List<CreateDeviceModel> devices)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userId = User.Identity.GetUserId();
            var claims = User as ClaimsPrincipal;

            return Ok();
        }

        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.NoContent, "Successfully removed device from registries.")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Device Id cannot be null.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "A problem occurred while trying to remove device from IoT Hub and Sql Azure.")]
        [Route("delete/{deviceId}")]
        public async Task<IHttpActionResult> Delete(int deviceId)
        {
            try
            {
                var device = await DbContext.Devices.FirstOrDefaultAsync(d => d.Id.Equals(deviceId));

                await DeviceRegistryManager.RemoveDeviceAsync(device.Code);

                DbContext.Devices.Remove(DbContext.Devices.FirstOrDefault(d => d.Id.Equals(deviceId)));
                DbContext.SaveChanges();

                return Content(HttpStatusCode.NoContent, string.Format(SuccessfullyDeleted, deviceId));
            }
            catch (DeviceNotFoundException)
            {
                return NotFound();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        #region Private Members

        private string PrepareIdStringToCode(string name)
        {
            return string.Concat(name.Replace(' ', '_'), "_", Guid.NewGuid().ToString());
        }

        private void CreateUpdateDeviceBlob()
        {
            try
            {
                var blobStorage =
                    new AzureStorage(ConfigurationManager.AppSettings.Get("as:AzureStorageConnectionString"));
                string deviceBasePath = HttpContext.Current.Server.MapPath(@"~\Temp");

                if (!Directory.Exists(deviceBasePath))
                    Directory.CreateDirectory(deviceBasePath);

                var blobContainer = blobStorage.BlobClient.GetContainerReference("devicereferencedata2");
                blobContainer.CreateIfNotExists();

                // Set Container Permissions
                var permissions = blobContainer.GetPermissions();
                permissions.PublicAccess = BlobContainerPublicAccessType.Container;
                blobContainer.SetPermissions(permissions);

                var now = DateTime.UtcNow;
                var blob = blobContainer.GetBlockBlobReference($"{now.ToString("yyyy-MM-dd")}/{now.ToString("HH:mm")}/devicesreferencedata2.json");
                //Add one minute to now
                if (blob.Exists())
                {
                    var forwardMinute = now.AddMinutes(1);
                    blob = blobContainer.GetBlockBlobReference($"{forwardMinute.ToString("yyyy-MM-dd")}/{forwardMinute.ToString("HH:mm")}/devicesreferencedata2.json");
                }

                var fileName = $@"{deviceBasePath}\devicesreferencedata2.json";
                var devices = DbContext.Devices.Include(d => d.SensorType).ToList();


                var deviceBytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(devices.Select(d => new
                {
                    sensorName = d.DeviceName,
                    deviceCode = d.Code,
                    sensorTypeId = d.SensorTypeId.ToString(),
                    sensorType = d.SensorType?.Value
                })));

                //Create file
                using (var fileStream = File.OpenWrite(fileName))
                {
                    //Settings
                    fileStream.WriteAsync(deviceBytes, 0, deviceBytes.Length);
                }
                //Upload file
                using (var fileStream = File.OpenRead(fileName))
                {
                    blob.UploadFromStream(fileStream);
                }
                //Delete file from disk
                File.Delete(fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<IHttpActionResult> GetDevices()
        {
            try
            {
                return Ok(await DeviceRegistryManager.GetDevicesAsync(int.Parse(ConfigurationManager.AppSettings[IoTMaxDevicesKey])));
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.Conflict, "Connection failed when trying to get devices from the IoT Hub");
            }
        }

        #endregion
    }
}

﻿using DomainModel.DTOs.Business;
using DomainModel.Entities.Business;
using Microsoft.AspNet.Identity;
using ProvisioningApi.Helpers;
using ServiceApi.Jwt;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ServiceApi.Controllers.ApiControllers
{
    [RoutePrefix("api/device-registrations")]
    public class DeviceRegistrationsController : BaseApiController
    {
        [JwtAuthorize(Roles = "User")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.Created, "Successfully sent mail for device registration.")]
        [Route("register-ownership")]
        public async Task<IHttpActionResult> RegisterOwnership(RegisterOwnershipDTO dto)
        {
            if (!ModelState.IsValidAndNotNull(dto))
            {
                return Content(HttpStatusCode.BadRequest, "Model provided is not valid.");
            }

            if (DevicesHaveBeenPreviouslyRegistered(DeviceCodesToSingleString(dto.DeviceCodes.OrderBy(s => s))))
            {
                return Content(HttpStatusCode.Conflict, "A registration has been already completed for the given device codes.");
            }

            var userId = HttpContext.Current.User.Identity.GetUserId();
            var confirmationToken = await UserManager.GenerateEmailConfirmationTokenAsync(userId);

            if (await AddUserDeviceRegistration(userId, confirmationToken, DeviceCodesToSingleString(dto.DeviceCodes.OrderBy(s => s))))
            {
                await UserManager.SendEmailAsync(userId,
                                "Confirm your account",
                                GetEmailBody(userId, confirmationToken, DeviceCodesToSingleString(dto.DeviceCodes.OrderBy(s => s))));

                return Created(new Uri(Url.Link("GetUserById", new { id = userId })), "Email for device registration sent.");
            }
            return InternalServerError();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("complete-device-registration", Name = "CompleteDeviceRegistration")]
        public async Task<IHttpActionResult> CompleteDeviceRegistration(string userId = "", string token = "", string deviceCodes = "")
        {
            if (string.IsNullOrWhiteSpace(userId) ||
                string.IsNullOrWhiteSpace(token) ||
                string.IsNullOrWhiteSpace(deviceCodes))
            {
                ModelState.AddModelError("", "User Id and Device Codes are required");
                return BadRequest(ModelState);
            }

            bool registrationSucceded = false;

            if (DevicesHaveBeenPreviouslyRegistered(deviceCodes))
            {
                registrationSucceded = await CompleteRegistrationForDevices(token, GetCodesFromMailLink(deviceCodes));
            }

            return registrationSucceded ? Content(HttpStatusCode.OK, "Successfully registered devices for the given codes.") :
                Content(HttpStatusCode.Conflict, "Devices have been already registered.");
        }

        #region Private Members

        private async Task<bool> AddUserDeviceRegistration(string userId, string token, string deviceCodes)
        {
            try
            {
                var registration = new UserDeviceRegistration
                {
                    ApplicationUserId = userId,
                    ConfirmationToken = token,
                    DeviceCodes = deviceCodes
                };

                DbContext.UserDeviceRegistrations.Add(registration);
                return await DbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private string GetEmailBody(string userId, string token, string deviceCodes)
        {
            var codes = deviceCodes.Split(new[] { ':' });
            string bulletedCodes = string.Empty;

            foreach (var c in codes)
            {
                bulletedCodes += string.Concat("<li>", c, "</li>");
            }

            return string.Format("Please confirm that you are trying to register the devices with the following codes: <ul>{0}</ul> By clicking <a href=\"{1}\">here</a>",
                bulletedCodes,
                new Uri(Url.Link("CompleteDeviceRegistration",
                                new
                                {
                                    userId = userId,
                                    token = token,
                                    deviceCodes = deviceCodes
                                })));
        }

        private bool DevicesHaveBeenPreviouslyRegistered(string deviceCodes)
        {
            var registration = DbContext.UserDeviceRegistrations.FirstOrDefault(udr => udr.DeviceCodes == deviceCodes);
            return registration != null && registration.IsCompleted;
        }

        private string DeviceCodesToSingleString(IEnumerable<string> deviceCodes)
        {
            return deviceCodes.Count() > 1 ? string.Join(":", deviceCodes) : deviceCodes.Single();
        }

        private ICollection<string> GetCodesFromMailLink(string deviceCodes)
        {
            return deviceCodes.Split(new[] { ':' });
        }

        private async Task<bool> CompleteRegistrationForDevices(string token, IEnumerable<string> codes)
        {
            var devices = new List<ApplicationDevice>();

            foreach (var c in codes)
            {
                var device = DbContext.Devices.FirstOrDefault(d => d.Code == c);
                device.LeasedOrSold = true;
            }

            if(await DbContext.SaveChangesAsync() > 0)
            {
                var registration = DbContext.UserDeviceRegistrations.First(udr => udr.ConfirmationToken == token);
                registration.IsCompleted = true;
                registration.CompletedOn = DateTime.Now;
                return true;
            }

            return false;
        }

        #endregion
    }
}
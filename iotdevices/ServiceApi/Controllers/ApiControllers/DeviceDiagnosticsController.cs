﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Http;

namespace ServiceApi.Controllers.ApiControllers
{
    [RoutePrefix("api/device-diagnostics")]
    public class DeviceDiagnosticsController : BaseApiController
    {
        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> DeviceDiagnostic()
        {
            var diagnostics = await DbContext.DeviceDiagnostics.ToListAsync();
            return Ok(diagnostics);
        }
    }
}
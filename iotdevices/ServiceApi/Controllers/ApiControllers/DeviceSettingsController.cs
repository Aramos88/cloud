﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ServiceApi.Libraries;
using System.Configuration;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using ServiceApi.Jwt;

namespace ServiceApi.Controllers.ApiControllers
{
    [JwtAuthorize(Roles = "SuperAdmin, Admin")]
    [RoutePrefix("api")]
    public class DeviceSettingsController : BaseApiController
    {
        private static readonly string _deviceBasePath = HttpContext.Current.Server.MapPath(@"~\Temp");
        private static readonly string _fileExtension = ".json";

        /// <summary>
        /// Add Device Settings stored in Azure block blob
        /// </summary>
        /// <param name="id"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("devices/{id}/settings")]
        public IHttpActionResult SetSettings(int id, object settings)
        {
            try
            {
                var serialized = JsonConvert.SerializeObject(settings);
                if (string.IsNullOrEmpty(serialized) || serialized == "null")
                    return BadRequest();

                CloudBlockBlob blob;
                GetBlob(id, out blob);
                
                //Active if you want consistency
                //if (!blob.Exists())
                //    return NotFound();

                var fileName = $@"{_deviceBasePath}\{id}{_fileExtension}";

                //Clear file!
                //File.WriteAllBytes(fileName, new byte[0]);
                using (var fileStream = File.OpenWrite(fileName))
                {                   
                    byte[] temp = new UTF8Encoding(true).GetBytes(serialized);
                    fileStream.Write(temp, 0, temp.Length);
                }
                //Upload to blob
                using (var fileStream = File.OpenRead(fileName))
                {
                    blob.UploadFromStream(fileStream);
                }
                //Delete file
                File.Delete(fileName);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return Ok(settings);
        }

        [HttpGet]
        [Route("v2/devices/{id}/settings")]
        public IHttpActionResult GetSettings(int id)
        {
            try
            {
                CloudBlockBlob blob;
                GetBlob(id, out blob);

                if (!blob.Exists())
                    return NotFound();

                //var fileName = $@"{_deviceBasePath}\{id}{_fileExtension}";
                var content = blob.DownloadText();

                if (string.IsNullOrEmpty(content))
                    return StatusCode(HttpStatusCode.NoContent);

                return Ok(JsonConvert.DeserializeObject(content));
                //Read file!
                /*using (StreamReader sr = new StreamReader(fileName))
                {
                    var settings = sr.ReadToEnd();

                    if (settings.Equals(string.Empty))
                        return StatusCode(HttpStatusCode.NoContent);

                    return Ok(JsonConvert.DeserializeObject(settings));
                }*/
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Get Download Link
        /// </summary>
        /// <param name="id"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("devices/{id}/settings/download")]
        public async Task<HttpResponseMessage> GetBlobSettings(int id)
        {
            try
            {
                CloudBlockBlob blob;
                GetBlob(id, out blob);
                
                if (!blob.Exists())
                    return new HttpResponseMessage(HttpStatusCode.NotFound);

                var ms = new MemoryStream();
                await blob.DownloadToStreamAsync(ms);

                // Reset the stream position; otherwise, download will not work
                ms.Position = 0;

                // Create response message with blob stream as its content
                var message = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(ms)
                };

                // Set content headers
                message.Content.Headers.ContentLength = blob.Properties.Length;
                message.Content.Headers.ContentType = new MediaTypeHeaderValue(blob.Properties.ContentType);
                message.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = HttpUtility.UrlDecode(id.ToString()),
                    Size = blob.Properties.Length
                };
                return message;
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
        /// <summary>
        /// Get Uri Link of Device Settings
        /// </summary>
        /// <param name="id"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("devices/{id}/settings")]
        public IHttpActionResult GetBlobSettingsUri(int id)
        {
            try
            {
                CloudBlockBlob blob;
                GetBlob(id, out blob);
                if (!blob.Exists())
                    return NotFound();
                return Ok(blob.Uri);
            }
            catch
            {
                return InternalServerError();
            }
        }

        private static void GetBlob(int id, out CloudBlockBlob blob)
        {
            try
            {
                var blobStorage =
                    new AzureStorage(ConfigurationManager.AppSettings.Get("as:AzureStorageConnectionString"));
                var blobContainer = blobStorage.BlobClient.GetContainerReference("devicesettings");
                blobContainer.CreateIfNotExists();
                var blobPath = $"{id}{_fileExtension}";
                blob = blobContainer.GetBlockBlobReference(blobPath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

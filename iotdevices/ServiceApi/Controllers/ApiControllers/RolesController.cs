﻿using System;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Net;
using Swashbuckle.Swagger.Annotations;
using System.Collections.Generic;
using DomainModel.Models.Identity;
using DomainModel.DTOs.Identity;
using ServiceApi.Jwt;

namespace ServiceApi.Controllers.ApiControllers
{
    [JwtAuthorize(Roles = "SuperAdmin, Admin")]
    [RoutePrefix("api/roles")]
    public class RolesController : BaseApiController
    {
        public RolesController()
            : base()
        {

        }

        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully fetched all roles from database.", typeof(IEnumerable<RoleViewModel>))]
        [SwaggerResponse(HttpStatusCode.Conflict, "Connection failed when trying to get roles from database.")]
        [Route("", Name = "GetAllRoles")]
        public IHttpActionResult GetAllRoles()
        {
            try
            {
                var roles = RoleManager.Roles.Select(r => new RoleViewModel
                {
                    Url = Url.Link("GetRoleById", new { id = r.Id }),
                    Id = r.Id,
                    Name = r.Name
                });

                return Ok(roles);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.Conflict, "Something went wrong and no roles were fetched.");
            }
        }

        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "A role with the given id was found.", typeof(RoleViewModel))]
        [SwaggerResponse(HttpStatusCode.NotFound, "No role with the given id was found.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "An exception was thrown.", typeof(Exception))]
        [Route("{id:guid}", Name = "GetRoleById")]
        public async Task<IHttpActionResult> GetRole(string id)
        {
            try
            {
                var role = await RoleManager.FindByIdAsync(id);

                if (role == null)
                {
                    return Content(HttpStatusCode.NotFound, "No role with the given id was found.");
                }

                if (role != null)
                {
                    return Ok(new RoleViewModel
                    {
                        Url = Url.Link("GetRoleById", new { id = role.Id }),
                        Id = role.Id,
                        Name = role.Name
                    });
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return null;
        }

        [HttpPost]
        [SwaggerResponse(HttpStatusCode.Created, "Successfully created and saved a new role to database.", typeof(RoleViewModel))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "The role name provided is not valid.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Something went wrong when trying to create the new role.")]
        [Route("create/{roleName}")]
        public async Task<IHttpActionResult> Create(string roleName)
        {
            if (!ModelState.IsValid || string.IsNullOrWhiteSpace(roleName))
            {
                return BadRequest(ModelState);
            }

            var role = new IdentityRole { Name = roleName };
            var result = await RoleManager.CreateAsync(role);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Created(new Uri(Url.Link("GetRoleById", new { id = role.Id })),
                new RoleViewModel
                {
                    Url = Url.Link("GetRoleById", new { id = role.Id}),
                    Id  = role.Id,
                    Name = role.Name
                });

        }

        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully deleted the role from the registries.")]
        [SwaggerResponse(HttpStatusCode.NotFound, "No role was found for the given Id.")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "The provided Id does not meet a proper format.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Something went wrong when trying to delete the role from database.")]
        [Route("{id:guid}")]
        public async Task<IHttpActionResult> DeleteRole(string id)
        {
            try
            {
                var role = await RoleManager.FindByIdAsync(id);

                if (role == null)
                {
                    return NotFound();
                }

                IdentityResult result = await RoleManager.DeleteAsync(role);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully managed the roles passed for the given users.")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Either a user was not found or removing/enrolling the user to the role failed.")]
        [SwaggerResponse(HttpStatusCode.NotFound, "No roles were found for the given id.")]
        [Route("manageUsersInRole")]
        public async Task<IHttpActionResult> ManageUsersInRole(UserRolesBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var role = await RoleManager.FindByIdAsync(model.RoleId);

            if (role == null)
            {
                return NotFound();
            }

            foreach (string userId in model.EnrolledUsers.Select(eu => eu.Id))
            {
                var appUser = await UserManager.FindByIdAsync(userId);

                if (appUser == null)
                {
                    ModelState.AddModelError("", string.Format("User: {0} does not exists", userId));
                    continue;
                }

                if (!UserManager.IsInRole(userId, role.Name))
                {
                    IdentityResult result = await UserManager.AddToRoleAsync(userId, role.Name);

                    if (!result.Succeeded)
                    {
                        ModelState.AddModelError("", string.Format("User: {0} could not be added to role", userId));
                        return BadRequest(ModelState);
                    }

                }
            }

            foreach (string userId in model.RevokedUsers.Select(ru => ru.Id))
            {
                var appUser = await UserManager.FindByIdAsync(userId);

                if (appUser == null)
                {
                    ModelState.AddModelError("", string.Format("User: {0} does not exists", userId));
                    continue;
                }

                IdentityResult result = await UserManager.RemoveFromRoleAsync(userId, role.Name);

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", string.Format("User: {0} could not be removed from role", userId));
                    return BadRequest(ModelState);
                }
            }

            return Ok();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using DomainModel.DTOs.Business;
using DomainModel.Entities.Business;
using Swashbuckle.Swagger.Annotations;
using DomainModel.Models.Business;
using DomainModel.Models.Devices;
using System.Data.Entity.Migrations;
using ServiceApi.Jwt;

namespace ServiceApi.Controllers.ApiControllers
{
    [RoutePrefix("api/subscriptions")]
    public class SubscriptionsController : BaseApiController
    {
        [JwtAuthorize(Roles = "SuperAdmin, Admin")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully added a new Subscription.", typeof(SubscriptionViewModel))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Model provided is not valid")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Could not find a user to link the subscription to.")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Cannot add a new subscription because an existing one was found for the given Email.")]
        [Route("")]
        public async Task<IHttpActionResult> Post(CreateSubscriptionDTO dto)
        {
            if (!ModelState.IsValid ||
                dto == null)
            {
                return Content(HttpStatusCode.BadRequest, "The model is not valid.");
            }

            var user = await UserManager.FindByEmailAsync(dto.ContactMail);

            if (user == null)
            {
                return Content(HttpStatusCode.NotFound, "Could not find a user to link the subscription to.");
            }

            return await AddNewSubscription(dto);
        }

        #region Private Members

        private async Task<IHttpActionResult> AddNewSubscription(CreateSubscriptionDTO dto)
        {
            if (DbContext.Subscriptions.Any(s => s.ContactMail == dto.ContactMail))
            {
                return Content(HttpStatusCode.Conflict, "Cannot add a new subscription because an existing one was found for the given Email.");
            }

            var devices = GetDevicesForSubscription(dto.DeviceCodes);

            if (devices.Count() == 0)
            {
                return Content(HttpStatusCode.NotFound, "No devices were found for the codes provided.");
            }

            var subscription = new Subscription
            {
                ContactMail = dto.ContactMail,
                NumberOfDevices = devices.Count(),
                PlanType = dto.SubscriptionPlan,
                Devices = devices.ToList(),
                BeginningDate = DateTime.Now,
                DueDate = DateTime.Now.AddMonths(1)
            };

            try
            {
                DbContext.Devices.AddOrUpdate(devices.ToArray());
                DbContext.Subscriptions.Add(subscription);
                await DbContext.SaveChangesAsync();

                return Ok(new SubscriptionViewModel
                {
                    Id = subscription.Id,
                    NumberOfDevices = subscription.NumberOfDevices,
                    BeginningDate = subscription.BeginningDate,
                    DueDate = subscription.DueDate,
                    Devices = subscription.Devices.Select(d => new ApplicationDeviceViewModel
                    {
                        Code = d.Code,
                        Description = d.Description
                    })
                });
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        private IEnumerable<ApplicationDevice> GetDevicesForSubscription(IEnumerable<string> codes)
        {
            var devices = new List<ApplicationDevice>();

            codes.Select(c =>
            {
                var d = DbContext.Devices.FirstOrDefault(ad => ad.Code == c);

                if (d != null)
                {
                    devices.Add(d);
                }

                return c;
            });

            return devices;
        }

        #endregion
    }
}

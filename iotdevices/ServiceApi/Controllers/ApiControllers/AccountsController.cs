﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Net.Http;
using System.Web;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;
using System.Net;
using System.Data.Entity;
using DomainModel.Entities.Identity;
using DomainModel.DTOs.Identity;
using DomainModel.Models.Identity;
using ServiceApi.Jwt;

namespace ServiceApi.Controllers.ApiControllers
{
    [RoutePrefix("api/accounts")]
    public class AccountsController : BaseApiController
    {
        [AllowAnonymous]
        [SwaggerResponse(HttpStatusCode.OK, "Login successfully granted", typeof(AuthTokenReturnModel))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Model provided is not valid")]
        [SwaggerResponse(HttpStatusCode.NotFound, "The given user with the password provided does not exist.")]
        [Route("login")]
        public async Task<IHttpActionResult> Login([FromBody] UserLoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = model.Username.Contains("@") ? await UserManager.FindByEmailAsync(model.Username) :
                await UserManager.FindAsync(model.Username, model.Password);

            if (user == null)
            {
                return NotFound();
            }

            var request = HttpContext.Current.Request;
            var tokenServiceUrl = string.Concat(request.Url.GetLeftPart(UriPartial.Authority),
                request.ApplicationPath,
                "oauth/token");

            using (var client = new HttpClient())
            {
                var tokenServiceResponse = await client.PostAsync(tokenServiceUrl,
                    new FormUrlEncodedContent(new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("grant_type", "password"),
                        new KeyValuePair<string, string>("username", model.Username),
                        new KeyValuePair<string, string>("password", model.Password)
                    }));

                var responseString = await tokenServiceResponse.Content.ReadAsStringAsync();

                var toReturn = JsonConvert.DeserializeObject<AuthTokenReturnModel>(responseString);

                return Ok(new AuthTokenReturnModel
                {
                    Username = user.UserName,
                    Email = user.Email,
                    AccessToken = toReturn.AccessToken,
                    TokenType = toReturn.TokenType,
                    ExpiresIn = DateTime.Now.AddDays(Convert.ToDouble(30)).ToString()
                });
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("confirm-email", Name = "ConfirmEmailRoute")]
        public async Task<IHttpActionResult> ConfirmEmail(string userId = "", string token = "")
        {
            if (string.IsNullOrWhiteSpace(userId) || 
                string.IsNullOrWhiteSpace(token))
            {
                ModelState.AddModelError("", "User Id and Code are required");
                return BadRequest(ModelState);
            }

            var result = await UserManager.ConfirmEmailAsync(userId, token);

            return result.Succeeded ? Content(HttpStatusCode.OK, "Successfully confirmed email.") :
                Content(HttpStatusCode.Conflict, "Not able to confirm the email with the given token.");
        }

        [AllowAnonymous]
        [SwaggerResponse(HttpStatusCode.Created, "Successfully registered a new user", typeof(UserProfile))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Model provided is not valid")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Something went wrong while registering a new user to the database")]
        [Route("create-user")]
        public async Task<IHttpActionResult> CreateUser(CreateUserBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser
            {
                UserName = model.Username,
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                JoinedOn = DateTime.Now,
                EmailConfirmed = true,
                Token = new Token
                {
                    AuthToken = Guid.NewGuid().ToString(),
                    IssuedOn = DateTime.Now,
                    ExpiresOn = DateTime.Now.AddDays(Convert.ToInt64(30))
                }
            };

            IdentityResult result = null;

            try
            {
                result = await UserManager.CreateAsync(user, model.Password);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }

                result = await UserManager.AddToRoleAsync(user.Id, "User");

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            return Created(new Uri(Url.Link("GetUserById", new { id = user.Id })),
                MapSingleToProfile(user));
        }

        [JwtAuthorize(Roles = "SuperAdmin")]
        [SwaggerResponse(HttpStatusCode.Created, "Successfully registered a new user", typeof(UserProfile))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Model provided is not valid")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Something went wrong while registering a new user to the database")]
        [Route("create-admin")]
        public async Task<IHttpActionResult> CreateAdmin(CreateUserBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser
            {
                UserName = model.Username,
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                JoinedOn = DateTime.Now,
                EmailConfirmed = true,
                Token = new Token
                {
                    AuthToken = Guid.NewGuid().ToString(),
                    IssuedOn = DateTime.Now,
                    ExpiresOn = DateTime.Now.AddDays(Convert.ToInt64(30))
                }
            };

            IdentityResult creationResult = null;
            IdentityResult addToRoleResult = null;

            try
            {
                creationResult = await UserManager.CreateAsync(user, model.Password);
                addToRoleResult = await UserManager.AddToRoleAsync(user.Id, "Admin");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            if (!creationResult.Succeeded ||
                !addToRoleResult.Succeeded)
            {
                return GetErrorResult(creationResult);
            }

            return Created(new Uri(Url.Link("GetUserById", new { id = user.Id })),
                MapSingleToProfile(user));
        }

        [JwtAuthorize]
        [SwaggerResponse(HttpStatusCode.NoContent, "Password successfully updated")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Model provided is not valid")]
        [Route("change-password")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NoContent));
        }

        [JwtAuthorize(Roles ="Admin")]
        [SwaggerResponse(HttpStatusCode.OK, "Acquire a list of all the users registered in the application", typeof(List<UserProfile>))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Something went wrong while getting records for all the users")]
        [Route("users")]
        public async Task<IHttpActionResult> GetUsers()
        {
            try
            {
                var users = await DbContext.Users.ToListAsync();
                return Ok(MapUsersToProfile(users));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [JwtAuthorize(Roles = "Admin")]
        [SwaggerResponse(HttpStatusCode.OK, "Get user info for the given Id", typeof(UserProfile))]
        [SwaggerResponse(HttpStatusCode.NotFound, "User for the given Id was not found")]
        [Route("user/{id:guid}", Name = "GetUserById")]
        public async Task<IHttpActionResult> GetUser(string id)
        {
            var user = await UserManager.FindByIdAsync(id);

            if (user != null)
            {
                return Ok(MapSingleToProfile(user));
            }

            return NotFound();
        }

        [JwtAuthorize(Roles = "Admin")]
        [SwaggerResponse(HttpStatusCode.OK, "Get user info for the given UserName", typeof(UserProfile))]
        [SwaggerResponse(HttpStatusCode.NotFound, "User with the given UserName wasn't found")]
        [Route("user/{username}")]
        public async Task<IHttpActionResult> GetUserByName(string username)
        {
            var user = await UserManager.FindByNameAsync(username);

            if (user != null)
            {
                return Ok(MapSingleToProfile(user));
            }

            return NotFound();
        }

        [JwtAuthorize(Roles = "Admin")]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.NoContent, "The user has been successfully removed from the database")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Not able to find the user with the given id")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Delete operation failed")]
        [Route("user/{id}")]
        public async Task<IHttpActionResult> DeleteUser(string id)
        {
            var appUser = await UserManager.FindByIdAsync(id);

            if (appUser != null)
            {
                var result = await UserManager.DeleteAsync(appUser);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NoContent));
            }

            return NotFound();
        }

        #region Private Methods

        private IEnumerable<UserProfile> MapUsersToProfile(IEnumerable<ApplicationUser> users)
        {
            try
            {
                IEnumerable<UserProfile> enumerableUsers = users.AsEnumerable().Select(au => new UserProfile
                {
                    Url = Url.Link("GetUserById", new { id = au.Id }),
                    Id = au.Id,
                    UserName = au.UserName,
                    FullName = string.Format("{0} {1}", au.FirstName, au.LastName),
                    Email = au.Email,
                    EmailConfirmed = au.EmailConfirmed,
                    JoinDate = au.JoinedOn,
                    Roles = UserManager.GetRolesAsync(au.Id).Result,
                    Claims = UserManager.GetClaimsAsync(au.Id).Result.Select(c => new ClaimsViewModel
                    {
                        Type = c.Type,
                        Value = c.Value,
                        ValueType = c.ValueType
                    })
                });

                return enumerableUsers;
            }
            catch (Exception ex) { return null; }
        }

        private UserProfile MapSingleToProfile(ApplicationUser user)
        {
            return new UserProfile
            {
                Url = Url.Link("GetUserById", new { id = user.Id }),
                Id = user.Id,
                UserName = user.UserName,
                FullName = string.Format("{0} {1}", user.FirstName, user.LastName),
                Email = user.Email,
                EmailConfirmed = user.EmailConfirmed,
                JoinDate = user.JoinedOn,
                Roles = UserManager.GetRolesAsync(user.Id).Result,
                Claims = UserManager.GetClaimsAsync(user.Id).Result.Select(c => new ClaimsViewModel
                {
                    Type = c.Type,
                    Value = c.Value,
                    ValueType = c.ValueType
                })
            };
        }

        #endregion
    }


}
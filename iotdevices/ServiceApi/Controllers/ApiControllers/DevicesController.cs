﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using Microsoft.Azure.Devices;
using Swashbuckle.Swagger.Annotations;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DomainModel.Models.Devices;
using System.Configuration;
using ServiceApi.Jwt;
using DomainModel.DTOs.Devices;
using DAL.Repositories.Base;
using System.Linq.Expressions;
using ProvisioningApi.Helpers;

namespace ServiceApi.Controllers.ApiControllers
{
    [JwtAuthorize(Roles = "SuperAdmin, Admin")]
    [RoutePrefix("api/devices")]
    public class DevicesController : BaseApiController
    {
        private const string IoTConnectionStringKey = "as:IoTConnectionString";

        private readonly string DeviceIdCannotBeNull;
        private readonly string SuccessfullyActivatedDevice;
        private readonly string SuccessfullyDeactivatedDevice;
        private readonly string DeviceOnSimulationMode;
        private readonly string DeviceNotFound;
        private readonly string AlreadySimulating;
        private readonly string InvalidModel;
        private readonly string DateTimeFormatString;
        private RegistryManager DeviceRegistryManager;
        private IRepository<DevicesData> Repository;

        public DevicesController()
        {
            DeviceIdCannotBeNull = "Failed to perform the operation on the device. DeviceId cannot be null or whitespace.";
            SuccessfullyActivatedDevice = "Successfully activated device with id {0}";
            SuccessfullyDeactivatedDevice = "Successfully deactivated device with id {0}";
            DeviceOnSimulationMode = "The device with id {0} is currently running on simulation mode.";
            DeviceNotFound = "Device for the given Id was not found";
            AlreadySimulating = "Device {0} is currently in simulation mode";
            InvalidModel = "Model did not pass validation or DeviceId not provided";
            DateTimeFormatString = "{0:yyyy-MM-ddThh:mm:ss}";
            DeviceRegistryManager = RegistryManager.CreateFromConnectionString(
                                    ConfigurationManager.AppSettings[IoTConnectionStringKey]);
            Repository = new DocumentDbRepository<DevicesData>();
        }

        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully deactivated device")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Device Id not provided")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Either the device was not found on the IoT Hub or there were connection problems while updating it.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Either an exception was thrown by the database or Updating IoTHub devices failed.")]
        [Route("activate/{deviceId}")]
        public async Task<IHttpActionResult> ActivateDevice(string deviceId)
        {
            if (string.IsNullOrWhiteSpace(deviceId))
            {
                return BadRequest(InvalidModel);
            }

            if (!await ActivateDeviceOnIoTHub(deviceId))
            {
                return Content(HttpStatusCode.Conflict, "Either the device was not found on the IoT Hub or there were connection problems while updating it.");
            }

            var device = DbContext.Devices.FirstOrDefault(d => d.DeviceName == deviceId);

            if (device.Status == DeviceStatus.Enabled)
            {
                return Ok(string.Format(SuccessfullyActivatedDevice, deviceId));
            }

            try
            {
                device.Status = DeviceStatus.Enabled;
                DbContext.Devices.AddOrUpdate(device);
                await DbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                InternalServerError(ex);
            }

            return Ok(string.Format(SuccessfullyActivatedDevice, deviceId));
        }

        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully deactivated device")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Device Id not provided")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Either the device was not found on the IoT Hub or there were connection problems while updating it.")]
        [Route("deactivate/{deviceId}")]
        public async Task<IHttpActionResult> DeactivateDevice(string deviceId)
        {
            if (string.IsNullOrWhiteSpace(deviceId))
            {
                return BadRequest(InvalidModel);
            }

            if (!await DeactivateDeviceOnIoTHub(deviceId))
            {
                return Content(HttpStatusCode.Conflict, "Either the device was not found on the IoT Hub or there were connection problems while updating it.");
            }

            var device = DbContext.Devices.FirstOrDefault(d => d.DeviceName == deviceId);

            if (device.Status == DeviceStatus.Disabled)
            {
                return Ok(string.Format(SuccessfullyDeactivatedDevice, deviceId));
            }

            try
            {
                device.Status = DeviceStatus.Disabled;
                DbContext.Devices.AddOrUpdate(device);
                await DbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            return Ok(string.Format(SuccessfullyDeactivatedDevice, deviceId));
        }

        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "Device running in simulation mode")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Device Id not provided")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Device with the given Id not found")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Exception thrown by the database")]
        [Route("simulate/{deviceId}")]
        public async Task<IHttpActionResult> RunSimulationMode(string deviceId)
        {
            if (string.IsNullOrWhiteSpace(deviceId))
            {
                return BadRequest(InvalidModel);
            }

            var device = DbContext.Devices.FirstOrDefault(d => d.DeviceName == deviceId);

            if (device == null)
            {
                return NotFound();
            }

            if (device.SimulationMode)
            {
                return Ok(string.Format(AlreadySimulating, deviceId));
            }

            try
            {
                device.SimulationMode = true;
                DbContext.Devices.AddOrUpdate(device);
                await DbContext.SaveChangesAsync();
            }
            catch (DataException ex)
            {
                return InternalServerError(ex);
            }

            return Ok(string.Format(DeviceOnSimulationMode, deviceId));
        }

        [HttpPut]
        [HttpPatch]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully updated the device")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Model did not pass validation")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Either the device was not found on the IoT Hub or there were connection problems while updating it.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Exception thrown by the database")]
        [Route("update")]
        public async Task<IHttpActionResult> UpdateDevice([FromBody] UpdateDeviceModel model)
        {
            if (!ModelState.IsValid ||
                model == null)
            {
                return BadRequest(InvalidModel);
            }

            if (!await UpdateDeviceOnIoTHub(model))
            {
                Content(HttpStatusCode.Conflict, "Either the device was not found on the IoT Hub or there were connection problems while updating it.");
            }

            return await UpdateDeviceEntity(model.Name, model);
        }

        #region Measurements

        [HttpGet]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Initial date is greater than the End date.")]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully fetched device measurements for the given initial and end dates.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Something went wrong while trying to fetch the data.")]
        [Route("measurements")]
        public async Task<IHttpActionResult> GetAll()
        {
            try
            {
                var result = await Repository.GetAllAsync();

                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        //[HttpGet]
        //[SwaggerResponse(HttpStatusCode.BadRequest, "Initial date is greater than the End date.")]
        //[SwaggerResponse(HttpStatusCode.OK, "Successfully fetched device measurements for the given initial and end dates.")]
        //[SwaggerResponse(HttpStatusCode.InternalServerError, "Something went wrong while trying to fetch the data.")]
        //[Route("{deviceId}/measurements")]
        //public async Task<IHttpActionResult> GetDeviceMeasurementsByDeviceId(int deviceId, int delimiter, DateTime initial, DateTime end, SortDirection orderType)
        //{
        //    try
        //    {
        //        if (initial > end)
        //        {
        //            return BadRequest("Initial is greater than end date");
        //        }

        //        var response = DbContext.DeviceMeasurements.Where(d => d.ApplicationDeviceId.Equals(deviceId) && d.SentAt >= initial && d.SentAt <= end);

        //        var list = orderType == SortDirection.Ascending ? response.OrderBy(d => d.SentAt).Take(delimiter).ToList() :
        //            response.OrderByDescending(d => d.SentAt).Take(delimiter).ToList();

        //        var objReturn = new
        //        {
        //            DevicesMeasurements = list,
        //            RangeDays = initial.GetTimeRangeForDateTimeDifference(end)
        //        };

        //        return Ok(objReturn);
        //    }
        //    catch (Exception ex)
        //    {
        //        return InternalServerError(ex);
        //    }
        //}

        [HttpGet]
        [SwaggerResponse(HttpStatusCode.BadRequest, "The given id is not valid or null.")]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully fetched the measurements for the given Device Id.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Something went wrong while trying to fetch the data.")]
        [Route("{deviceId}/measurements")]
        public async Task<IHttpActionResult> GetMeasurementById(int deviceId, int delimiter = 10)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(deviceId.ToString()) ||
                    string.IsNullOrEmpty(deviceId.ToString()))
                {
                    return BadRequest();
                }

                Expression<Func<DevicesData, bool>> predicate = dd => dd.ApplicationDeviceId == deviceId;

                var result = await Repository.FindAsync(predicate);

                if (result.Count() == 0)
                {
                    return NotFound();
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }

        [HttpPost]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Model provided is not valid or null.")]
        [SwaggerResponse(HttpStatusCode.Created, "Successfully persisted data to the document database.")]
        [Route("measurements/create")]
        public async Task<IHttpActionResult> CreateAsync(DevicesData model)
        {
            if (!ModelState.IsValidAndNotNull(model))
            {
                return BadRequest();
            }

            await Repository.AddAsync(model);
            return Content(HttpStatusCode.Created, model);
        }

        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Id provided is not valid.")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Could not find a register to delete.")]
        [SwaggerResponse(HttpStatusCode.NoContent, "Successfully deleted the registry for the given Id.")]
        [Route("{id}/measurements")]
        public async Task<IHttpActionResult> DeleteAsync(string id)
        {
            if (string.IsNullOrEmpty(id) ||
                string.IsNullOrWhiteSpace(id))
            {
                return BadRequest();
            }

            var result = await Repository.RemoveAsync(id);

            return result != null ? Content(HttpStatusCode.NoContent, result) : Content(HttpStatusCode.NotFound, default(DevicesData));
        }

        [HttpDelete]
        [Route("all/measurements")]
        public async Task<IHttpActionResult> DeleteDocumentsAsync()
        {
            try
            {
                await Repository.RemoveAllAsync();
                return Content(HttpStatusCode.NoContent, "");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        #endregion

        #region Filter Device
        [HttpGet]
        [Route("{id}")]
        [ResponseType(typeof(Device))]
        public async Task<IHttpActionResult> GetDeviceById(int id)
        {

            var devices = await DbContext.Devices.Select(d => d).Include(dt => dt.SensorType)
                                                 .Where(st => st.Id.Equals(id))
                                                 .ToListAsync();

            if (devices == null)
                return NotFound();

            return Ok(devices);

        }

        [HttpGet]
        [AllowAnonymous]
        [Route("")]
        [ResponseType(typeof(Device))]
        public async Task<IHttpActionResult> GetDevices(string type = null)
        {

            var devices = await DbContext.Devices.Select(d => d).Include(dt => dt.SensorType)
                                                 .Where(st => type == null || st.SensorType.Value.Equals(type))
                                                 .ToListAsync();

            if (devices == null)
                return NotFound();

            return Ok(devices);

        }
        #endregion

        #region Private Members

        private async Task<bool> ActivateDeviceOnIoTHub(string deviceId)
        {
            var device = await DeviceRegistryManager.GetDeviceAsync(deviceId);

            if (device == null)
            {
                return false;
            }

            if (device.Status == DeviceStatus.Enabled)
            {
                return true;
            }

            try
            {
                device.Status = DeviceStatus.Enabled;
                await DeviceRegistryManager.UpdateDeviceAsync(device);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private async Task<bool> DeactivateDeviceOnIoTHub(string deviceId)
        {
            var device = await DeviceRegistryManager.GetDeviceAsync(deviceId);

            if (device == null)
            {
                return false;
            }

            if (device.Status == DeviceStatus.Disabled)
            {
                return true;
            }

            try
            {
                device.Status = DeviceStatus.Disabled;
                await DeviceRegistryManager.UpdateDeviceAsync(device);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private async Task<bool> UpdateDeviceOnIoTHub(UpdateDeviceModel model)
        {
            var device = await DeviceRegistryManager.GetDeviceAsync(model.Name);

            if (device == null)
            {
                return false;
            }

            try
            {
                device.ETag = string.IsNullOrWhiteSpace(model.ETag) ? device.ETag : model.ETag;
                device.StatusReason = string.IsNullOrWhiteSpace(model.StatusReason) ? device.StatusReason : model.StatusReason;
                device.Status = string.IsNullOrWhiteSpace(model.Status.ToString()) ? device.Status : model.Status;

                await DeviceRegistryManager.UpdateDeviceAsync(device);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private async Task<IHttpActionResult> UpdateDeviceEntity(string deviceId, UpdateDeviceModel model)
        {
            var device = DbContext.Devices.FirstOrDefault(d => d.DeviceName == deviceId);

            device.SimulationMode = string.IsNullOrWhiteSpace(model.SimulationMode.ToString()) ? device.SimulationMode : model.SimulationMode;
            device.CurrentState = string.IsNullOrWhiteSpace(model.CurrentState.ToString()) ? device.CurrentState : model.CurrentState;
            device.SensorTypeId = string.IsNullOrWhiteSpace(model.SensorId.ToString()) ? device.SensorTypeId : model.SensorId;

            try
            {
                DbContext.Devices.AddOrUpdate(device);
                await DbContext.SaveChangesAsync();
            }
            catch (DataException ex)
            {
                return InternalServerError(ex);
            }

            return Ok(string.Format(DeviceOnSimulationMode, deviceId));
        }

        #endregion
    }
}

﻿using System;
using System.Net;
using System.Web.Http;
using Swashbuckle.Swagger.Annotations;
using System.Threading.Tasks;
using System.Linq;
using DomainModel.Entities.Business;
using DomainModel.DTOs.Devices;

namespace ServiceApi.Controllers.ApiControllers
{
    [RoutePrefix("api/sensors")]
    public class SensorTypesController : BaseApiController
    {

        [Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            var devices = DbContext.SensorTypes.AsQueryable();

            return Ok(devices);
        }

        [SwaggerResponse(HttpStatusCode.Created, "Successfully registered a new sensor type.", typeof(SensorType))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Invalid model or null.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Something went wrong when registering sensor type to database.")]
        [Route("register")]
        public async Task<IHttpActionResult> RegisterNew(RegisterSensorBindingModel model)
        {
            if (!ModelState.IsValid ||
                model == null)
            {
                return BadRequest();
            }

            try
            {
                var sensor = DbContext.SensorTypes.Add(new SensorType
                {
                    Value = model.Name
                });

                await DbContext.SaveChangesAsync();

                return Created("api/sensors", sensor);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
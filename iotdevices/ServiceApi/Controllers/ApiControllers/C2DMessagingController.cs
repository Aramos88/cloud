﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using DomainModel.Entities.Business;
using Microsoft.AspNet.Identity;
using ServiceApi.Jwt;
using ServiceApi.Libraries;
using Microsoft.Azure;
using System.Net;
using Swashbuckle.Swagger.Annotations;
using ProvisioningApi.Helpers;

namespace ServiceApi.Controllers.ApiControllers
{
    [JwtAuthorize(Roles ="SuperAdmin, Admin")]
    [RoutePrefix("api/c2dmessaging")]
    public class C2DMessagingController : BaseApiController
    {
        private StreamAnalyticsClient SAClient;
        private C2DMessageBroker C2DMessageBroker;

        public C2DMessagingController()
        {
            SAClient = new StreamAnalyticsClient();
            C2DMessageBroker = new C2DMessageBroker();
        }

        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "Creation of Stream Analytics job succeded.")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Creation of Stream Analytics job failed to respond as created or running.")]
        [Route("query-streaming-analitics")]
        public IHttpActionResult CreateOutput()
        {
            var response = SAClient.GenerateOutput();

            return response.Status == OperationStatus.Succeeded ? Content(HttpStatusCode.OK, "Creation of Stream Analytics job succeded.") :
                Content(HttpStatusCode.Conflict, "Creation of Stream Analytics job failed to respond as created or running.");
        }

        [HttpPost]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Cloud to device message model not valid or null.")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Device not found for the given Id.")]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully sent message and received feedback from device.")]
        [SwaggerResponse(HttpStatusCode.NoContent, "Successfully sent message but did not receive feedback from device.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Creation of Stream Analytics job failed to respond as created or running.")]
        [Route("send-message-c2d")]
        public async Task<IHttpActionResult> SendMessageC2D(C2DArduinoMessage message)
        {
            try
            {
                if (!ModelState.IsValidAndNotNull(message))
                {
                    return BadRequest(ModelState);
                }

                message.ApplicationUserId = RequestContext.Principal.Identity.GetUserId();
                var device = DbContext.Devices.FirstOrDefault(s => s.Id.Equals(message.ApplicationDeviceId));

                if (device == null)
                {
                    return NotFound();
                }

                message.ResponseFeedBack = await C2DMessageBroker.SendMessageC2D(device.DeviceName, message);
                DbContext.C2DArduinoMessages.Add(message);
                DbContext.SaveChanges();

                return message.ResponseFeedBack ? Content(HttpStatusCode.OK, "Successfully sent message and received feedback from device.") : 
                    Content(HttpStatusCode.NoContent, "Successfully sent message but did not receive feedback from device.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
﻿using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using DAL.Context;
using System.Diagnostics;

namespace ServiceApi.Controllers.ApiControllers
{
    public class BaseApiController : ApiController
    {
        protected ApplicationUserManager UserManager;
        protected ApplicationRoleManager RoleManager;
        protected ApplicationDbContext DbContext;

        protected BaseApiController()
        {
            UserManager = HttpContext.Current.Request.GetOwinContext().Get<ApplicationUserManager>();
            RoleManager = HttpContext.Current.Request.GetOwinContext().Get<ApplicationRoleManager>();
            DbContext = HttpContext.Current.Request.GetOwinContext().Get<ApplicationDbContext>();
            DbContext.Database.Log = s => Debug.WriteLine(s);
        }

        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) DbContext.Dispose();
            base.Dispose(disposing);
        }
    }
}
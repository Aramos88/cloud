﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Swashbuckle.Swagger.Annotations;
using ServiceApi.DocumentDb;
using ServiceApi.Swag.Attributes;
using ServiceApi.DocumentDb.Enums;
using DomainModel.DTOs.Devices;
using ServiceApi.Jwt;

namespace ServiceApi.Controllers.ApiControllers
{
    [JwtAuthorize]
    [RoutePrefix("api/measurementstats")]
    public class MeasurementStatsController : ApiController
    {
        private MeasurementStatsAnalyzer StatsAnalyzer;

        public MeasurementStatsController()
        {
            StatsAnalyzer = new MeasurementStatsAnalyzer();
        }

        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully fetched measurement statistics.", typeof(GatewayMeasurement))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "The Id provided is not valid.")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Something went wrong and no statistics were fetched at all.")]
        [SwaggerMultipleResponseTypes(typeof(GatewayMeasurement), typeof(DeviceMeasurementsExamples))]
        [Route("lasthour/{deviceId}")]
        public async Task<IHttpActionResult> GetForLastHour(string deviceId)
        {
            int id;

            if (!int.TryParse(deviceId, out id))
            {
                return Content(HttpStatusCode.BadRequest, "The Id provided is not valid.");
            }

            var stats = await StatsAnalyzer.GetStatsForTimeWindow(id, DateTime.UtcNow, TimeWindowOptions.LastDay);

            if (stats == null)
            {
                return Content(HttpStatusCode.Conflict, "Something went wrong when trying to fetch the measurements statistics.");
            }

            return Ok(stats);
        }

        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully fetched measurement statistics.", typeof(GatewayMeasurement))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "The Id provided is not valid.")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Something went wrong and no statistics were fetched at all.")]
        [SwaggerMultipleResponseTypes(typeof(GatewayMeasurement), typeof(DeviceMeasurementsExamples))]
        [Route("lastday/{deviceId}")]
        public async Task<IHttpActionResult> GetForLastDay(string deviceId)
        {
            int id;

            if (!int.TryParse(deviceId, out id))
            {
                return Content(HttpStatusCode.BadRequest, "The Id provided is not valid.");
            }

            var stats = await StatsAnalyzer.GetStatsForTimeWindow(id, DateTime.UtcNow, TimeWindowOptions.LastDay);

            if (stats == null)
            {
                return Content(HttpStatusCode.Conflict, "Something went wrong when trying to fetch the measurements statistics.");
            }

            return Ok(stats);
        }

        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully fetched measurement statistics.", typeof(GatewayMeasurement))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "The Id provided is not valid.")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Something went wrong and no statistics were fetched at all.")]
        [SwaggerMultipleResponseTypes(typeof(GatewayMeasurement), typeof(DeviceMeasurementsExamples))]
        [Route("lastweek/{deviceId}")]
        public async Task<IHttpActionResult> GetForLastWeek(string deviceId)
        {
            int id;

            if (!int.TryParse(deviceId, out id))
            {
                return Content(HttpStatusCode.BadRequest, "The Id provided is not valid.");
            }

            var stats = await StatsAnalyzer.GetStatsForTimeWindow(id, DateTime.UtcNow, TimeWindowOptions.LastDay);

            if (stats == null)
            {
                return Content(HttpStatusCode.Conflict, "Something went wrong when trying to fetch the measurements statistics.");
            }

            return Ok(stats);
        }
    }
}
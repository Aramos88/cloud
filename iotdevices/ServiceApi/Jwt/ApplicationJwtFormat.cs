﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using System;
using System.Data.Entity;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity.Migrations;
using DAL.Context;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IdentityModel.Tokens;

namespace ServiceApi.Jwt
{
    public class ApplicationJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private const double ExpirationTokenPolicy = 30;
        private readonly string Issuer;
        private readonly string AudienceIdName;
        private readonly string AudienceSecretName;

        public ApplicationJwtFormat()
        {
            AudienceIdName = "as:AudienceId";
            AudienceSecretName = "as:AudienceSecret";
        }

        public ApplicationJwtFormat(string issuer)
            : this()
        {
            Issuer = issuer;
        }

        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            var token = new JwtSecurityTokenHandler().WriteToken(new JwtSecurityToken(Issuer,
                ConfigurationManager.AppSettings[AudienceIdName],
                data.Identity.Claims,
                data.Properties.IssuedUtc.Value.UtcDateTime,
                data.Properties.ExpiresUtc.Value.UtcDateTime,
                new HmacSigningCredentials(TextEncodings.Base64Url.Decode(ConfigurationManager.AppSettings[AudienceSecretName]))));

            return ProcessOutboundToken(token, data.Identity.Claims.ToArray()[0].Value);
        }

        private string ProcessOutboundToken(string token, string userId)
        {
            using (var context = HttpContext.Current.Request.GetOwinContext().Get<ApplicationDbContext>())
            {
                var user = context.Users.Include(au => au.Token).FirstOrDefault(au => au.Id == userId);
                user.Token.AuthToken = token;
                user.Token.IssuedOn = DateTime.Now;
                user.Token.ExpiresOn = DateTime.Now.AddDays(ExpirationTokenPolicy);

                try
                {
                    context.Users.AddOrUpdate(user);
                    context.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}",
                                                    validationError.PropertyName,
                                                    validationError.ErrorMessage);
                        }
                    }
                }
            }

            return token;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}
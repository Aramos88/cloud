﻿using System;
using DAL.Context;
using System.Linq;
using System.Web.Http.Controllers;
using ServiceApi.Helpers;
using System.Web.Http;
using System.Security.Claims;

namespace ServiceApi.Jwt
{
    public sealed class JwtAuthorize : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var headerToken = actionContext.Request.GetFirstHeaderValueOrDefault<string>("Authorization")?
                .Split(new[] { "Bearer" }, StringSplitOptions.RemoveEmptyEntries)[0]
                .Trim();

            var compliesWithRole = (actionContext.RequestContext.Principal.Identity
                                     as ClaimsIdentity)
                                                    .Claims
                                                    .Any(c => c.Value.Any(Roles.Contains)) ||
                                                    !Roles.Any();

            if (string.IsNullOrEmpty(headerToken) ||
                !compliesWithRole)
            {
                HandleUnauthorizedRequest(actionContext);
                return false;
            }
           
            bool tokenExists;

            using (var db = new ApplicationDbContext())
            {
                tokenExists = db.Tokens.Any(t => t.AuthToken == headerToken);
            }

            if (!tokenExists)
            {
                HandleUnauthorizedRequest(actionContext);
                return false;
            }

            return base.IsAuthorized(actionContext);
        }
    }
}
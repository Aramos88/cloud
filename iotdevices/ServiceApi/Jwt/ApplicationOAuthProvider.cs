﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using DAL.Context;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using DomainModel.Entities.Identity;

namespace ServiceApi.Jwt
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string InvalidGrantName;
        private readonly string IncorrectPassOrUserName;
        private readonly string UserDidNotConfirm;
        private readonly string[] AllowedOrigins;
        private readonly string JwtName;
        private readonly string AccessControlHeadersName;

        public ApplicationOAuthProvider()
        {
            InvalidGrantName = "invalid_grant";
            IncorrectPassOrUserName = "The user name or password is incorrect.";
            UserDidNotConfirm = "User has not confirmed his/her email yet.";
            AllowedOrigins = new[] { "*" };
            JwtName = "JWT";
            AccessControlHeadersName = "Access-Control-Allow-Origin";
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add(AccessControlHeadersName, AllowedOrigins);

            ApplicationUser user = await context.OwinContext.Get<ApplicationUserManager>()
                .FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError(InvalidGrantName, IncorrectPassOrUserName);
                return;
            }

            if (!user.EmailConfirmed)
            {
                context.SetError(InvalidGrantName, UserDidNotConfirm);
                return;
            }

            var ticket = new AuthenticationTicket(await user.GenerateUserIdentityAsync(
                context.OwinContext.Get<ApplicationUserManager>(), JwtName),
                CreateProperties(user.UserName, user.Id));

            context.Validated(ticket);
        }

        public static AuthenticationProperties CreateProperties(string userName, string id)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "UserName", userName },
                { "UserId", id }
            };
            return new AuthenticationProperties(data);
        }
    }
}
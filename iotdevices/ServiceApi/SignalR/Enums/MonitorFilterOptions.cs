﻿namespace ServiceApi.SignalR.Enums
{
    public enum MonitorFilterOptions
    {
        None,
        ByApplicationDeviceId,
        BySensorTypeId
    };
}
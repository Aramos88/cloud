﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using DAL.Context;
using System.Configuration;
using ServiceApi.SignalR.Hubs;
using DomainModel.Entities.Business;

namespace ServiceApi.SignalR.Common
{
    public class DeviceMeasurementTicker
    {
        // Singleton instance
        private readonly static Lazy<DeviceMeasurementTicker> _instance = new Lazy<DeviceMeasurementTicker>(() => new DeviceMeasurementTicker(GlobalHost.ConnectionManager.GetHubContext<DeviceMeasurementsHub>().Clients));

        private readonly object _lock = new object();

        public Timer Timer = null;
        
        private readonly TimeSpan _updateInterval = TimeSpan.FromMilliseconds(int.Parse(ConfigurationManager.AppSettings.Get("IntervalRefresh")));        
        //private volatile bool _updatingData = false;
        private static int _deviceMeasurementId = 0;
        private static int _deviceId = 0;

        public static DeviceMeasurementTicker Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        private IHubConnectionContext<dynamic> Clients { get; set; }

        public DeviceMeasurementTicker(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;
        }

        public void Initialize(int deviceId)
        {
            _deviceId = deviceId;
            _deviceMeasurementId = 0;
            Timer = new Timer(UpdateDeviceMeasurements, null, _updateInterval, _updateInterval);
        }

        private void UpdateDeviceMeasurements(object state)
        {
            lock (_lock)
            {
                // GET LAST DEVICE MEASUREMENTS
                foreach (var stock in GetLatestDataByDeviceId(_deviceId, 1))
                {   
                    BroadcastDeviceMeasurement(stock);
                    //_deviceMeasurementId = stock.Id;
                }
            }
        }

        public List<DeviceMeasurement> GetLatestDataByDeviceId(int deviceId, int delimiter)
        {
            using (var db = new ApplicationDbContext())
            {
                var dateTime = DateTime.UtcNow.AddHours(int.Parse(ConfigurationManager.AppSettings.Get("HoursMinus")));
                var result = db.DeviceMeasurements.Where(d => d.ApplicationDeviceId == deviceId && 
                        d.Id > _deviceMeasurementId).
                        OrderByDescending(d => d.SentAt).Take(delimiter).ToList();
                _deviceMeasurementId = result.Count() == 0 ? _deviceMeasurementId : result.First().Id;
                System.Diagnostics.Trace.WriteLine("Executing Query " + DateTime.Now);
                System.Diagnostics.Debug.WriteLine("_deviceMeasurementId: " + _deviceMeasurementId);
                System.Diagnostics.Debug.WriteLine("_deviceId: " + _deviceId);
                return result;
            }
        }

        private void BroadcastDeviceMeasurement(DeviceMeasurement stock)
        {
            Clients.All.updateDeviceMeasurement(stock);
        }
    }
}
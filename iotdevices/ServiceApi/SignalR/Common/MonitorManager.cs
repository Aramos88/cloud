﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Configuration;
using System.Collections.Generic;
using Microsoft.Azure.Devices.Common;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json.Linq;
using DAL.Context;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR;
using ServiceApi.SignalR.Hubs;
using ServiceApi.SignalR.Enums;
using DomainModel.DTOs.Enums;
using DomainModel.DTOs.Devices;
using Newtonsoft.Json;

namespace ServiceApi.SignalR.Common
{
    public class MonitorManager
    {
        private static long EpochValue = 50;
        private static long EpochIncrement = 10;
        private static long ExistingEpochValue;
        private readonly static Lazy<MonitorManager> _instance = new Lazy<MonitorManager>(
                                                                    () => new MonitorManager(GlobalHost
                                                                        .ConnectionManager
                                                                        .GetHubContext<AdminMonitorHub>()
                                                                        .Clients));
        private readonly string IoTConnectionStringKey;
        private readonly string IotHubD2cEndpoint;
        private readonly string FieldGatewayKey;
        private readonly string MonitorConsumerGroupKey;
        private readonly string ReceiverTimespanPolicyKey;
        private readonly string MonitorCallbackPeriodKey;
        private readonly string MonitorCallbackDueTimeKey;
        private readonly double ReceiverTimespan;
        private readonly string FieldGatewayName;
        private readonly string MonitorConsumerGroupName;
        private readonly int TimerCallbackDueTime;
        private readonly int TimerCallbackPeriod;

        private readonly EventHubClient _hubClient;
        private EventHubReceiver _receiver;
        private readonly object _receiverLock = new object();
        private readonly object _lock = new object();

        private IHubConnectionContext<dynamic> Clients { get; set; }

        private int DeviceId { get; set; }

        private int SensorTypeId { get; set; }

        private string ReceivingDeviceCode { get; set; }

        private MonitorFilterOptions FilterOption { get; set; }

        public Timer Timer { get; set; }

        public static MonitorManager Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        public MonitorManager()
        {
            IoTConnectionStringKey = "as:IoTConnectionString";
            IotHubD2cEndpoint = "messages/events";
            FieldGatewayKey = "as:FieldGatewayName";
            MonitorConsumerGroupKey = "as:MonitorConsumerGroup";
            ReceiverTimespanPolicyKey = "as:MonitorReceiverTimespanPolicy";
            MonitorCallbackPeriodKey = "as:MonitorCallbackPeriod";
            MonitorCallbackDueTimeKey = "IntervalRefresh";

            _hubClient = EventHubClient.CreateFromConnectionString(ConfigurationManager.AppSettings[IoTConnectionStringKey],
                IotHubD2cEndpoint);
            FieldGatewayName = ConfigurationManager.AppSettings[FieldGatewayKey];
            MonitorConsumerGroupName = ConfigurationManager.AppSettings[MonitorConsumerGroupKey];
            ReceiverTimespan = Convert.ToDouble(ConfigurationManager.AppSettings[ReceiverTimespanPolicyKey]);
            TimerCallbackDueTime = Convert.ToInt32(ConfigurationManager.AppSettings[MonitorCallbackDueTimeKey]);
            TimerCallbackPeriod = Convert.ToInt32(ConfigurationManager.AppSettings[MonitorCallbackPeriodKey]);
        }

        public MonitorManager(IHubConnectionContext<dynamic> clients)
            : this()
        {
            Clients = clients;
        }

        public void SetMonitorParameters(MonitorFilterOptions filter, string deviceId = null, string sensorTypeId = null)
        {
            FilterOption = filter;
            DeviceId = string.IsNullOrEmpty(deviceId) ? 0 : int.Parse(deviceId);
            SensorTypeId = string.IsNullOrEmpty(sensorTypeId) ? 0 : int.Parse(sensorTypeId);
        }

        public void CreateReceiverForDeviceCode()
        {
            bool firstTryFailed = false;

            try
            {
                using (var db = new ApplicationDbContext())
                {
                    ReceivingDeviceCode = db.Devices.FirstOrDefault(d => d.Id == DeviceId).Code;
                }

                _receiver = _hubClient.GetConsumerGroup(MonitorConsumerGroupName).CreateReceiver(EventHubPartitionKeyResolver.ResolveToPartition(ReceivingDeviceCode,
                _hubClient.GetRuntimeInformation().PartitionCount),
                DateTime.UtcNow,
                EpochValue);
            }
            catch (Exception ex)
            {
                firstTryFailed = true;
            }

            if (firstTryFailed)
            {
                EpochValue = ExistingEpochValue + EpochIncrement;
                _receiver = _hubClient.GetConsumerGroup(MonitorConsumerGroupName).CreateReceiver(EventHubPartitionKeyResolver.ResolveToPartition(ReceivingDeviceCode,
                _hubClient.GetRuntimeInformation().PartitionCount), DateTime.UtcNow, EpochValue);
            }

            Timer = new Timer(GetEventsFromIoTHub, null, TimerCallbackDueTime, TimerCallbackPeriod);
        }

        private void GetEventsFromIoTHub(object state)
        {
            lock (_lock)
            {
                foreach (var e in ProcessEvents(FilterOption, DeviceId, SensorTypeId))
                {
                    if (e != null)
                    {
                        BroadcastEventData(e);
                    }
                }
            }
        }

        private IEnumerable<object> ProcessEvents(MonitorFilterOptions filter, int deviceId, int sensorTypeId)
        {
            EventData eventData = null;
            bool originalReceiverFailed = false;

            try
            {
                lock (_receiverLock)
                {
                    eventData = _receiver.Receive(TimeSpan.FromMilliseconds(ReceiverTimespan));
                }
            }
            catch (Exception ex)
            {
                originalReceiverFailed = true;
            }

            if (originalReceiverFailed)
            {
                EpochValue = ExistingEpochValue + EpochIncrement;
                _receiver = _hubClient.GetConsumerGroup(MonitorConsumerGroupName).CreateReceiver(EventHubPartitionKeyResolver.ResolveToPartition(ReceivingDeviceCode,
                _hubClient.GetRuntimeInformation().PartitionCount), DateTime.UtcNow, EpochValue);

                lock (_receiverLock)
                {
                    eventData = _receiver.Receive(TimeSpan.FromMilliseconds(ReceiverTimespan));
                }
            }

            if (eventData != null)
            {
                var jsonString = Encoding.UTF8.GetString(eventData.GetBytes());

                switch (filter)
                {
                    case MonitorFilterOptions.None:
                        {
                            yield return GetMeasurementsForDevice(deviceId, jsonString);
                            break;
                        }
                    case MonitorFilterOptions.ByApplicationDeviceId:
                        {
                            yield return GetMeasurementsForDevice(deviceId, jsonString);
                            break;
                        }
                    case MonitorFilterOptions.BySensorTypeId:
                        {
                            yield return GetMeasurementsForDevice(deviceId, jsonString);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }

            yield return null;
        }

        private void BroadcastEventData(object eventData)
        {
            Clients.All.updateDeviceMeasurement(eventData);
        }

        private object GetMeasurementsForDevice(int deviceId, string jsonString)
        {
            int sensorTypeId = 0;

            using (var db = new ApplicationDbContext())
            {
                sensorTypeId = db.Devices.FirstOrDefault(d => d.Id == deviceId).SensorTypeId;
            }

            return DeserializeStringToJsObject(jsonString, (SensorTypes)sensorTypeId);
        }

        private object DeserializeStringToJsObject(string jsonString, SensorTypes sensorType)
        {
            switch (sensorType)
            {
                case SensorTypes.Color:
                    {
                        var model = JsonConvert.DeserializeObject<RgbMeasurement>(jsonString);

                        return model.DeviceInfoId == DeviceId ? JObject.FromObject(model,
                            SignalRContractResolver.GetJsonSerializer()) : null;
                    }
                case SensorTypes.XZ_Plane_Distance:
                    {
                        var model = JsonConvert.DeserializeObject<XZDistanceMeasurement>(jsonString);

                        return model.DeviceInfoId == DeviceId ? JObject.FromObject(model,
                            SignalRContractResolver.GetJsonSerializer()) : null;
                    }
                default:
                    {
                        var model = JsonConvert.DeserializeObject<GatewayMeasurement>(jsonString);

                        return model.DeviceInfoId == DeviceId ? JObject.FromObject(model,
                            SignalRContractResolver.GetJsonSerializer()) : null;
                    }
            }
        }
    }
}
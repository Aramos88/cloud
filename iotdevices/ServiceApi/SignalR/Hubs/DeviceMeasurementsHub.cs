﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using ServiceApi.SignalR.Common;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceApi.SignalR.Hubs
{

    /// <summary>
    /// 
    /// </summary>
    [HubName("deviceMeasurements")]
    public class DeviceMeasurementsHub : Hub
    {
        public DeviceMeasurementTicker _stockTicker;

        public DeviceMeasurementsHub() : this(DeviceMeasurementTicker.Instance) { }
        public DeviceMeasurementsHub(DeviceMeasurementTicker stockTicker)
        {
            _stockTicker = stockTicker;
        }

        /// <summary>
        /// Aqui estan los cambios
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="delimiter"></param>
        [HubMethodName("getDeviceData")]
        public void GetDeviceMeasurements(int deviceId, int delimiter = 20)
        {
            _stockTicker.Initialize(deviceId);
            var result =_stockTicker.GetLatestDataByDeviceId(deviceId, delimiter);
            Clients.All.initializeDeviceMeasurement(result.OrderBy(d => d.SentAt));            
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            _stockTicker.Timer.Dispose();
            //custom logic here
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            return base.OnReconnected();
        }
    }
}
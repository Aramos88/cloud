﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using ServiceApi.SignalR.Common;
using ServiceApi.SignalR.Enums;

namespace ServiceApi.SignalR.Hubs
{
    [HubName("adminMonitor")]
    public class AdminMonitorHub : Hub
    {
        private CancellationTokenSource _cts;

        public MonitorManager _manager;

        public AdminMonitorHub()
            : this(MonitorManager.Instance)
        {
            _cts = new CancellationTokenSource();
        }

        public AdminMonitorHub(MonitorManager instance)
        {
            _manager = instance;
        }

        //TODO
        [HubMethodName("getAllMeasurements")]
        public void GetAllMeasurements()
        {
            _manager.SetMonitorParameters(MonitorFilterOptions.None);
            _manager.CreateReceiverForDeviceCode();
        }

        [HubMethodName("getAllForDevice")]
        public void GetAllForDevice(string deviceId)
        {
            _manager.SetMonitorParameters(MonitorFilterOptions.ByApplicationDeviceId, deviceId);
            _manager.CreateReceiverForDeviceCode();
        }

        //TODO
        [HubMethodName("getAllForSensorType")]
        public void GetAllForSensorTypeId(string sensorTypeId)
        {
            _manager.SetMonitorParameters(MonitorFilterOptions.BySensorTypeId, sensorTypeId: sensorTypeId);
            _manager.CreateReceiverForDeviceCode();
        }

        public override Task OnReconnected()
        {
            return base.OnReconnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            _manager.Timer.Dispose();
            return base.OnDisconnected(stopCalled);
        }
    }
}
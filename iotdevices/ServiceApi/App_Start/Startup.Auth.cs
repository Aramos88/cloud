﻿using Microsoft.Owin;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Configuration;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security;
using DAL.Context;
using ServiceApi.Jwt;

namespace ServiceApi
{
    public partial class Startup
    {
        private const double TokenExpirationPolicy = 30;
        private readonly string AudienceIdName = "as:AudienceId";
        private readonly string AudienceSecretName = "as:AudienceSecret";
        private readonly string Issuer = @"http://localhost:51769/";

        public void ConfigureAuth(IAppBuilder app)
        {
            RegisterOwinContextCallbacks(app);
            ConfigureOAuthTokenGeneration(app);
            ConfigureOAuthTokenConsumption(app);
        }

        private void RegisterOwinContextCallbacks(IAppBuilder app)
        {
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);
        }

        private void ConfigureOAuthTokenGeneration(IAppBuilder app)
        {
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                //TODO For Dev environment only (on production should be AllowInsecureHttp = false)
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/oauth/token"),
                //AuthorizeEndpointPath = new PathString("/api/accounts/login"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(TokenExpirationPolicy),
                Provider = new ApplicationOAuthProvider(),
                AccessTokenFormat = new ApplicationJwtFormat(Issuer)
            };

            // OAuth 2.0 Bearer Access Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
        }

        private void ConfigureOAuthTokenConsumption(IAppBuilder app)
        {
            //TODO Controllers with an [Authorize] attribute will be validated with JWT
            app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AllowedAudiences = new[] { ConfigurationManager.AppSettings[AudienceIdName] },
                IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new SymmetricKeyIssuerSecurityTokenProvider(Issuer,
                        TextEncodings.Base64Url.Decode(ConfigurationManager.AppSettings[AudienceSecretName]))
                    }
            });
        }
    }
}
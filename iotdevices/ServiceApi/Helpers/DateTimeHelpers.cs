﻿using System;
using System.Collections.Generic;

namespace ServiceApi.Helpers
{
    public static class DateTimeHelpers
    {
        public static Dictionary<string, int> GetTimeRangeForDateTimeDifference(this DateTime initial, DateTime end)
        {
            var range = end - initial;
            var timeRange = new Dictionary<string, int>();

            int yearsTime = range.Days / 365;
            int monthsTime = (range.Days - (yearsTime * 365)) / 31;
            int weeksTime = ((range.Days - (monthsTime * 31)) - yearsTime * 365) / 7;
            int daysTime = (((range.Days - yearsTime * 365) - (monthsTime * 31)) - weeksTime * 7);

            timeRange.Add("minutes", range.Minutes);
            timeRange.Add("hours", range.Hours);
            timeRange.Add("days", daysTime);
            timeRange.Add("week", weeksTime);
            timeRange.Add("months", monthsTime);
            timeRange.Add("years", yearsTime);

            return timeRange;
        }
    }
}
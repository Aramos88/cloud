﻿using System.Web.Http.ModelBinding;

namespace ProvisioningApi.Helpers
{
    public static class ModelStateHelpers
    {
        public static bool IsValidAndNotNull(this ModelStateDictionary modelState, object model)
        {
            return modelState.IsValid && model != null;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace ServiceApi.Helpers
{
    public static class HttpRequestExtensions
    {
        public static T GetFirstHeaderValueOrDefault<T>(this HttpRequestMessage request,
            string headerKey)
        {
            var toReturn = default(T);
            IEnumerable<string> headerValues;

            if (!request.Headers.TryGetValues(headerKey, out headerValues))
            {
                return toReturn;
            }

            var valueString = headerValues.FirstOrDefault();

            if (valueString == null)
            {
                return toReturn;
            }

            return (T)Convert.ChangeType(valueString, typeof(T));
        }
    }
}
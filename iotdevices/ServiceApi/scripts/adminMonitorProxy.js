﻿$(function () {

    var $counter = 0;

    function populateData(measurement) {

        if (measurement) {
            $counter++;
            $('#counter').text($counter);
        }

        switch(measurement.deviceInfoId)
        {
            case 6:
                {
                    $('#measurements tbody').append('<tr><td>' + measurement.deviceInfoId + '</td><td>' + measurement.sentAt + '</td><td>' + "r: " + measurement.data.measure.r + " g: " + measurement.data.measure.g + " b: " + measurement.data.measure.b + " " + measurement.data.units + '</td></tr>');
                    break;
                }
            case 10:
                {
                    $('#measurements tbody').append('<tr><td>' + measurement.deviceInfoId + '</td><td>' + measurement.sentAt + '</td><td>' + "x: " + measurement.data.measure.x + " z: " + measurement.data.measure.z + " " + measurement.data.units + '</td></tr>');
                    break;
                }
            default:
                {
                    $('#measurements tbody').append('<tr><td>' + measurement.deviceInfoId + '</td><td>' + measurement.sentAt + '</td><td>' + measurement.measure + " " + measurement.unit + '</td></tr>');
                    break;
                }
        }
    }

    $(document).ready(function () {

        var hubConnected = false;
        var hubConnection = $.connection.hub.stop();
        var adminHub = $.connection.adminMonitor;
        var $selected = 0;
        var $sensor;
        var $device;

        function startPolling(deviceId, filter) {
            hubConnected = true;
            // START POLLING

            switch(filter)
            {
                case 'all': {
                    hubConnection = $.connection.hub.start().done(function () {
                        adminHub.server.getAllMeasurements();
                    });
                    break;
                }
                case 'byDeviceId': {
                    hubConnection = $.connection.hub.start().done(function () {
                        adminHub.server.getAllForDevice(deviceId);
                        });
                    break;
                }
                case 'bySensorTypeId': {
                    hubConnection = $.connection.hub.start().done(function () {
                        adminHub.server.getAllForSensorType(deviceId);
                    });
                    break;
                }
                default:
                    break;
            }
        }

        $('#stop').on('click', function (e) {
            //e.preventDefault();
            if (hubConnected) {
                $('#stop').text('Iniciar');
                hubConnected = false;
                hubConnection = $.connection.hub.stop()
            } else {
                $('#stop').text('Detener');
                startPolling();
            }
        });

        $('#filter').on('change', function () {

            var $filter = $('#filter option:selected').val();
            $('#measurements tbody').html('');
            console.log($filter);

            /**
             * SingalR init
             */
            adminHub.client.initializeDeviceMeasurement = function (measurements) {
                measurements.forEach(function (measurement, key) {
                    populateData(measurement);
                });
            };

            adminHub.client.updateDeviceMeasurement = function (measurement) {
                populateData(measurement);
            };

            startPolling($selected, $filter);
        });

        var sensors = $.getJSON('../../api/sensors', function (data) {

            $sensor = $('#sensor');

            $.each(data, function (key, value) {
                var $option = $('<option>');
                $sensor.append($option.val(value.id).text(value.value));
            });
            $('select').material_select();

            $sensor.on('change', function () {

                $selected = $('#sensor option:selected').val();
                $filter = $('#filter option:selected').val();
                $('#measurements tbody').html('');
                console.log($selected);
                //hubConnection = $.connection.hub.stop();

                /**
                 * SingalR init
                 */
                adminHub.client.initializeDeviceMeasurement = function (measurements) {
                    measurements.forEach(function (measurement, key) {
                        populateData(measurement);
                    });
                };

                adminHub.client.updateDeviceMeasurement = function (measurement) {
                    populateData(measurement);
                };

                startPolling($selected, $filter);
            });

        });

        var devices = $.getJSON('../../api/devices', function (data) {
            $device = $('#device');
            $.each(data, function (key, value) {
                var $option = $('<option>');
                $device.append($option.val(value.id).text(value.deviceName));
            });
            $('select').material_select();
            $device.on('change', function () {

                $selected = $('#device option:selected').val();
                $filter = $('#filter option:selected').val();
                $('#measurements tbody').html('');
                console.log($selected);
                //hubConnection = $.connection.hub.stop();

                /**
                 * SingalR init
                 */
                adminHub.client.initializeDeviceMeasurement = function (measurements) {
                    measurements.forEach(function (measurement, key) {
                        populateData(measurement);
                    });
                };

                adminHub.client.updateDeviceMeasurement = function (measurement) {
                    populateData(measurement);
                };

                startPolling($selected, $filter);
            });
        });
    });
});
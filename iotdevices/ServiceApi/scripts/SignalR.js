﻿$(function () {
    function populateData(measurement) {
        $('#measurements tbody').append('<tr><td>' + measurement.id + '</td><td>' + measurement.sentAt + '</td><td>' + measurement.data + '</td></tr>');
    }
    
    $(document).ready(function () {

        var hubConnected = false;
        var hubConnection = $.connection.hub.stop();
        var measurementHub = $.connection.deviceMeasurements;
        var $selected = 0;
        var delimiter = 10;

        function startPolling() {
            hubConnected = true;
            // START POLLING
            hubConnection = $.connection.hub.start().done(function () {
                measurementHub.server.getDeviceData($selected, delimiter);
            });
        }

        $('#stop').on('click', function (e) {
            //e.preventDefault();
            if (hubConnected) {
                $('#stop').text('Iniciar');
                hubConnected = false;
                hubConnection = $.connection.hub.stop()
            } else {
                $('#stop').text('Detener');
                startPolling();
            }
        });

        var devices = $.getJSON('../../api/devices', function (data) {
            var $device = $('#device');
            $.each(data, function (key, value) {
                var $option = $('<option>');
                $device.append($option.val(value.id).text(value.deviceName));
            });
            $('select').material_select();
            $device.on('change', function () {

                $selected = $('#device option:selected').val();
                $('#measurements tbody').html('');
                console.log($selected);
                hubConnection = $.connection.hub.stop();

                /**
                 * SingalR init
                 */
                measurementHub.client.initializeDeviceMeasurement = function (measurements) {
                    console.log(measurements);

                    measurements.forEach(function (measurement, key) {
                        populateData(measurement);
                    });
                };

                measurementHub.client.updateDeviceMeasurement = function (measurement) {
                    console.log('Update', measurement);
                    populateData(measurement);
                };

                startPolling();
            });
        });
    });

    

    //var hubTemp = $.connection.hub.start().done(function () {

    //    measurementTemp.server.getDeviceData(1);
    //});

    //var hubHumidity = $.connection.hub.start().done(function () {
    //    measurementHumidity.server.getDeviceData(9);
    //});
    //});
});
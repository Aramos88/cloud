﻿using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace ServiceApi.Libraries
{
    public class AzureStorage
    {
        private CloudStorageAccount StorageAccount { get; set; }
        public CloudBlobClient BlobClient { get; }

        public AzureStorage(string storageConnection)
        {
            //Set storage Account credentials
            StorageAccount = CloudStorageAccount.Parse(storageConnection);
            BlobClient = StorageAccount.CreateCloudBlobClient();
        }

        public CloudBlockBlob Upload(byte[] file, string directory, string filename)
        {
            // Create the container (Directory)
            var container = BlobClient.GetContainerReference(directory);
            container.CreateIfNotExists();
            container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

            CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);
            using (var stream = new MemoryStream(file))
            {
                blockBlob.UploadFromStream(stream);
            }
            return blockBlob;
        }

        public string ContentFile(string directory, string filename)
        {
            // Retrieve reference to a previously created container.
            var container = BlobClient.GetContainerReference(directory);
            var block = container.GetBlockBlobReference(filename);
            if (!block.Exists()) return string.Empty;
            return block.DownloadText(System.Text.Encoding.Unicode);
        }
    }
}
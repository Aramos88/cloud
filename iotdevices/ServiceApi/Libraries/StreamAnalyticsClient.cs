﻿using System;
using Microsoft.Azure;
using Microsoft.Azure.Management.StreamAnalytics;
using Microsoft.Azure.Management.StreamAnalytics.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace ServiceApi.Libraries
{
    public class StreamAnalyticsClient
    {
        public StreamAnalyticsClient()
        {

        }

        public LongRunningOperationResponse GenerateOutput()
        {
            var resourceGroupName = "IotStart";
            var client = GetClient();
            var streamAnalyticsJobName = "iotStreamJob";

            var jobCreateParams = new JobCreateOrUpdateParameters
            {
                Job = new Job
                {
                    Name = "Job",
                    Location = "North Europe",
                    Properties = new JobProperties
                    {
                        EventsOutOfOrderPolicy = EventsOutOfOrderPolicy.Adjust,
                        Sku = new Sku { Name = "Standard" }
                    }
                }
            };
            client.StreamingJobs.CreateOrUpdate(resourceGroupName, jobCreateParams);

            var jobInputCreateParams = new InputCreateOrUpdateParameters
            {
                Input = new Input
                {
                    Name = "Job Input",
                    Properties = new StreamInputProperties
                    {
                        Serialization = new JsonSerialization
                        {
                            Properties = new JsonSerializationProperties
                            {
                                Encoding = "UTF8"
                            }
                        },
                        DataSource = new EventHubStreamInputDataSource
                        {
                            Properties = new EventHubStreamInputDataSourceProperties
                            {
                                EventHubName = "IotHubStart",
                                ConsumerGroupName = resourceGroupName,
                                ServiceBusNamespace = "thingseventhub-ns",
                                SharedAccessPolicyName = "listen",
                                SharedAccessPolicyKey = "x|+r4hHAL3ogF1jBgma7Qc9eStWsQ="
                            }
                        }
                    }
                }
            };

            client.Inputs.CreateOrUpdate(resourceGroupName, streamAnalyticsJobName, jobInputCreateParams);

            client.Inputs.TestConnection(resourceGroupName, streamAnalyticsJobName, "IotHubMsgSrc");

            var jobOutputCreateParameters = new OutputCreateOrUpdateParameters
            {
                Output = new Output
                {
                    Name = "Job Output",
                    Properties = new OutputProperties
                    {
                        Serialization = new JsonSerialization
                        {
                            Properties = new JsonSerializationProperties
                            {
                                Encoding = "UTF8",
                                Format = "LineSeparated"
                            }
                        },
                        DataSource = new EventHubOutputDataSource
                        {
                            Properties = new EventHubOutputDataSourceProperties
                            {
                                EventHubName = "IotHubStart",
                                ServiceBusNamespace = "thingseventhub-ns",
                                SharedAccessPolicyName = "send",
                                SharedAccessPolicyKey = "RHe07bejSead0jxi+r4hHAL3ogF1jBgma7Qc9eStWsQ="
                            }
                        }
                    }
                }
            };

            client.Outputs.CreateOrUpdate(resourceGroupName, streamAnalyticsJobName, jobOutputCreateParameters);

            client.Outputs.TestConnection(resourceGroupName, streamAnalyticsJobName, "Job Output");

            var transformationCreateParameters = new TransformationCreateOrUpdateParameters
            {
                Transformation = new Transformation
                {
                    Name = "streamAnalyticsTransformationName",
                    Properties = new TransformationProperties
                    {
                        StreamingUnits = 1,
                        Query = "SELECT * INTO [Job Output] FROM [Job Input]"
                    }
                }
            };

            client.Transformations.CreateOrUpdate(resourceGroupName, streamAnalyticsJobName, transformationCreateParameters);

            var jobStartParameters = new JobStartParameters
            {
                OutputStartMode = OutputStartMode.CustomTime,
                OutputStartTime = DateTime.UtcNow
            };

            return client.StreamingJobs.Start(resourceGroupName, streamAnalyticsJobName, jobStartParameters);
        }

        private StreamAnalyticsManagementClient GetClient()
        {
            string SubscriptionId = "ffe202f2-d6a8-448a-8e85-8527801d5050";

            var aadTokenCredentials = new TokenCloudCredentials(SubscriptionId, GetAuthorizationHeader());
            return new StreamAnalyticsManagementClient(aadTokenCredentials);
        }

        private string GetAuthorizationHeader()
        {
            string WindowsManagementUri = "https://management.core.windows.net/";
            string AsaClientId = "cc2ba943-d502-4d44-93cc-301587619b8b";
            string AppKey = "cmLI88JU1Nr5LaAkPuUOdCxNM5HAM/dVU2gLx/dT520=";
            string ActiveDirectoryTenantId = "5fd41e3e-19da-4e8a-9773-7c150e4b0137";

            AuthenticationResult authenticationResult = null;

            try
            {
                var clientCredential = new ClientCredential(AsaClientId, AppKey);
                var authenticationContext = new AuthenticationContext("https://login.microsoftonline.com/" + ActiveDirectoryTenantId + "/oauth2/token");
                authenticationResult = authenticationContext.AcquireToken(WindowsManagementUri, clientCredential);
            }
            catch (Exception ex) { }
            return authenticationResult.AccessToken;
        }
    }
}
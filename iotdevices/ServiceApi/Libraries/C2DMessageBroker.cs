﻿using DomainModel.Entities.Business;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Common.Exceptions;
using System;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceApi.Libraries
{
    public class C2DMessageBroker
    {
        private static string ConnectionStringKey = "as:IoTConnectionString";
        private ServiceClient ServiceClient;

        public C2DMessageBroker()
        {
            ServiceClient = ServiceClient.CreateFromConnectionString(ConfigurationManager.AppSettings[ConnectionStringKey]);
        }

        //Change MessageC2D to C2DArduinoMessage
        public async Task<bool> SendMessageC2D(string deviceName, C2DArduinoMessage message)
        {
            try
            {
                //var commandMessage = new Message(Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(message.BodyMessage)));
                var commandMessage = new Message(Encoding.ASCII.GetBytes(message.ToString()));
                commandMessage.Ack = DeliveryAcknowledgement.Full;

                var confirmReceived = ServiceClient.GetFeedbackReceiver();
                var received = false;

                var tokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(int.Parse(ConfigurationManager.AppSettings.Get("TimeCancellationToken"))));

                var sendMessageTask = Task.Run(async () =>
                {
                    await ServiceClient.SendAsync(deviceName, commandMessage);
                });

                var feedbackTask = Task.Run(async () =>
                {
                    while (!received && !tokenSource.IsCancellationRequested)
                    {
                        var feedbackBatch = await confirmReceived.ReceiveAsync(TimeSpan.FromSeconds(0.5));
                        if (feedbackBatch != null)
                        {
                            received = feedbackBatch.Records.Any(f => f.Description != null);
                            if (received)
                            {
                                await confirmReceived.CompleteAsync(feedbackBatch);
                                break;
                            }
                        }
                    }
                }, tokenSource.Token);

                await Task.WhenAll(sendMessageTask, feedbackTask);
                return received;
            }
            catch (UnauthorizedException ex) { return false; }
        }
    }
}
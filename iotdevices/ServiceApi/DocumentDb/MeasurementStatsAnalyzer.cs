﻿using System;
using System.Linq;
using DAL.Context;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System.Diagnostics;
using ServiceApi.DocumentDb.Enums;
using DomainModel.DTOs.Enums;
using DomainModel.DTOs.Devices;

namespace ServiceApi.DocumentDb
{
    public class MeasurementStatsAnalyzer
    {
        private readonly string DatabaseId;
        private readonly string CollectionId;
        private readonly string DatetimeFormatString;
        private readonly string StartDateParameterName;
        private readonly string EndDateParameterName;
        private readonly string DeviceIdParameterName;

        private static List<dynamic> Stats = new List<dynamic>();
        private DocumentClient Client;

        public MeasurementStatsAnalyzer()
        {
            DatabaseId = ConfigurationManager.AppSettings["ddb:database"];
            CollectionId = ConfigurationManager.AppSettings["ddb:collection"];
            Client = new DocumentClient(new Uri(ConfigurationManager.AppSettings["ddb:Endpoint"]),
                ConfigurationManager.AppSettings["ddb:AuthKey"],
                new ConnectionPolicy
                {
                    ConnectionMode = ConnectionMode.Direct,
                    ConnectionProtocol = Protocol.Tcp,
                    MaxConnectionLimit = 200
                },
                ConsistencyLevel.Eventual);
            DatetimeFormatString = "yyyy-MM-dd HH:mm:ss";
            StartDateParameterName = "@startDate";
            EndDateParameterName = "@endDate";
            DeviceIdParameterName = "@deviceId";
        }

        public async Task<IEnumerable<object>> GetStatsForTimeWindow(int deviceId, DateTime now, TimeWindowOptions timeWindow)
        {
            int sensorTypeId;
            var startDate = SetInitialTimeWindowStart(now, timeWindow);
            var endDate = SetTimeWindowEnd(startDate, timeWindow);

            using (var db = new ApplicationDbContext())
            {
                sensorTypeId = db.Devices.FirstOrDefault(d => d.Id == deviceId).SensorTypeId;
            }

            var queries = new List<IDocumentQuery<dynamic>>();

            while (endDate <= now)
            {
                queries.Add(GetAggregateFunctionQuery(deviceId, startDate, endDate, DocumentDbAggregateFunctions.Min));
                queries.Add(GetAggregateFunctionQuery(deviceId, startDate, endDate, DocumentDbAggregateFunctions.Max));
                startDate = endDate.AddSeconds(Convert.ToDouble(1));
                endDate = SetTimeWindowEnd(startDate, timeWindow);
            }

            return Parallel.ForEach(queries, (q) => ExecuteDocumentDbQueries(q)).IsCompleted ?
                ConvertDynamicToDto(Stats.OrderBy(m => m.receivedat), (SensorTypes)sensorTypeId) : null;
       }

        private IDocumentQuery<dynamic> GetAggregateFunctionQuery(int deviceId, DateTime startDate, DateTime endDate, DocumentDbAggregateFunctions function)
        {
            string query = string.Empty;

            switch (function)
            {
                case DocumentDbAggregateFunctions.Min:
                    {
                        query = @"SELECT TOP 1 VALUE {deviceid: DM.deviceinfoid, receivedat: DM.receivedat, measurement: DM.data.measure, units: DM.data.unit}
                                FROM deviceMeasurements AS DM WHERE DM.deviceinfoid = @deviceId
                                AND DM._ts > @startDate AND DM._ts < @endDate ORDER BY DM.data.measure";
                        break;
                    }
                case DocumentDbAggregateFunctions.Max:
                    {
                        query = @"SELECT TOP 1 VALUE {deviceid: DM.deviceinfoid, receivedat: DM.receivedat, measurement: DM.data.measure, units: DM.data.unit}
                                FROM deviceMeasurements AS DM WHERE DM.deviceinfoid = @deviceId
                                AND DM._ts > @startDate AND DM._ts < @endDate ORDER BY DM.data.measure DESC";
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            return Client.CreateDocumentQuery(UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId),
                                    new SqlQuerySpec
                                    {
                                        QueryText = query,
                                        Parameters = new SqlParameterCollection
                                        {
                                            new SqlParameter(EndDateParameterName, GetTimespan(endDate)),
                                            new SqlParameter(StartDateParameterName, GetTimespan(startDate)),
                                            new SqlParameter(DeviceIdParameterName, deviceId)
                                        }
                                    },
                                    new FeedOptions
                                    {
                                        MaxDegreeOfParallelism = -1,
                                        MaxBufferedItemCount = -1
                                    })
                                    .AsDocumentQuery();
        }

        private long GetTimespan(DateTime date)
        {
            return (long)Math.Floor((date - DateTime.SpecifyKind(new DateTime(1970, 1, 1), DateTimeKind.Utc)).TotalSeconds);
        }

        private DateTime SetInitialTimeWindowStart(DateTime now, TimeWindowOptions timeWindow)
        {
            switch (timeWindow)
            {
                case TimeWindowOptions.LastHour:
                    {
                        return now.AddHours(Convert.ToDouble(-1));
                    }
                case TimeWindowOptions.LastDay:
                    {
                        return now.AddDays(Convert.ToDouble(-1));
                    }
                case TimeWindowOptions.LastWeek:
                    {
                        return now.AddDays(Convert.ToDouble(-7));
                    }
                default:
                    {
                        return now;
                    }
            }
        }

        private DateTime SetTimeWindowEnd(DateTime date, TimeWindowOptions timeWindow)
        {
            switch (timeWindow)
            {
                case TimeWindowOptions.LastHour:
                    {
                        return date.AddMinutes(Convert.ToDouble(1));
                    }
                case TimeWindowOptions.LastDay:
                    {
                        return date.AddMinutes(Convert.ToDouble(20));
                    }
                case TimeWindowOptions.LastWeek:
                    {
                        return date.AddHours(Convert.ToDouble(2));
                    }
                default:
                    {
                        return date;
                    }
            }
        }

        private void ExecuteDocumentDbQueries(IDocumentQuery<dynamic> query)
        {
            try
            {
                var input = query.ExecuteNextAsync().GetAwaiter().GetResult();

                if (input.Count != 0)
                {
                    Stats.Add(input.Single());
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        private IEnumerable<object> ConvertDynamicToDto(IEnumerable<dynamic> input, SensorTypes sensorType)
        {
            switch (sensorType)
            {
                case SensorTypes.Color:
                    {
                        return input.Select(m => new RgbMeasurement
                        {
                            DeviceInfoId = Convert.ToInt32(m.deviceid),
                            SentAt = m.receivedat.ToLocalTime().ToString(DatetimeFormatString),
                            Data = new RgbData
                            {
                                Measure = new RgbMeasure
                                {
                                    r = Convert.ToInt32(m.measurement.r),
                                    g = Convert.ToInt32(m.measurement.g),
                                    b = Convert.ToInt32(m.measurement.b)
                                },
                                Units = m.units
                            }
                        });
                    }
                case SensorTypes.XZ_Plane_Distance:
                    {
                        return input.Select(m => new XZDistanceMeasurement
                        {
                            DeviceInfoId = Convert.ToInt32(m.deviceid),
                            SentAt = m.receivedat.ToLocalTime().ToString(DatetimeFormatString),
                            Data = new XZDistanceData
                            {
                                Measure = new XZMeasure
                                {
                                    x = m.measurement.x,
                                    z = m.measurement.z
                                },
                                Units = m.units
                            }
                        });
                    }
                default:
                    {
                        return input.Select(m => new GatewayMeasurement
                        {
                            DeviceInfoId = Convert.ToInt32(m.deviceid),
                            Measure = m.measurement,
                            Unit = m.units
                        });
                    }
            }
        }
    }
}
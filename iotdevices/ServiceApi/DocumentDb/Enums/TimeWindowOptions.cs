﻿
namespace ServiceApi.DocumentDb.Enums
{
    public enum TimeWindowOptions
    {
        LastHour,
        LastDay,
        LastWeek
    };
}
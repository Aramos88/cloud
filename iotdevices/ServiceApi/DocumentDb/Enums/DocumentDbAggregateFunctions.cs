﻿
namespace ServiceApi.DocumentDb.Enums
{
    public enum DocumentDbAggregateFunctions
    {
        Min,
        Max
    };
}
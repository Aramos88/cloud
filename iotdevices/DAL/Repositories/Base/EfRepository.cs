﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DAL.Repositories.Base
{
    public class EfRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        protected readonly DbContext Context;

        public EfRepository(DbContext context)
        {
            Context = context;
        }

        public TEntity Get(object id)
        {
            return Context.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Context.Set<TEntity>().ToList();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await Context.Set<TEntity>().ToListAsync();
        }

        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().FirstOrDefault(predicate);
        }

        public void Add(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);
        }

        public async Task AddAsync(TEntity entity)
        {
            await Task.Factory.StartNew(() =>
            {
                Context.Set<TEntity>().Add(entity);
            });
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            Context.Set<TEntity>().AddRange(entities);
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Where(predicate).AsEnumerable();
        }

        public async Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await Context.Set<TEntity>().Where(predicate).ToListAsync();
        }

        public TEntity Remove(object id)
        {
            return Context.Set<TEntity>().Remove(Get(id));
        }

        public async Task<TEntity> RemoveAsync(object id)
        {
            return await Task.Factory.StartNew(() =>
            {
                return Context.Set<TEntity>().Remove(Get(id));
            });
        }

        public IEnumerable<TEntity> RemoveRange(IEnumerable<object> ids)
        {
            var entities = new List<TEntity>();

            foreach (var id in ids)
            {
                entities.Add(Get(id));
            }

            return Context.Set<TEntity>().RemoveRange(entities);
        }

        public async Task<IEnumerable<TEntity>> RemoveRangeAsync(IEnumerable<object> ids)
        {
            return await Task.Factory.StartNew(() =>
            {
                var entities = new List<TEntity>();

                foreach (var id in ids)
                {
                    entities.Add(Get(id));
                }

                return Context.Set<TEntity>().RemoveRange(entities);
            });
        }

        public async Task RemoveAllAsync()
        {
            await Task.Factory.StartNew(() =>
            {
                Context.Set<TEntity>().RemoveRange(Context.Set<TEntity>());
            });
        }
    }
}

﻿using System;
using System.Net;
using System.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Azure.Documents.Client;

namespace DAL.Repositories.Base
{
    public class DocumentDbRepository<TColl> : IRepository<TColl>
        where TColl : class
    {
        private static string DatabaseIdKeyName = "ddb:database";
        private static string DeviceMeasurementsCollectionIdKey = "ddb:collDeviceMeasurements";
        private static string EndpointKeyName = "ddb:Endpoint";
        private static string AuthKeyName = "ddb:AuthKey";

        private readonly string DatabaseId;
        private readonly string CollectionId;
        private readonly string Endpoint;
        private readonly string AuthKey;
        private readonly Uri CollectionUri;
        private DocumentClient Client;

        public DocumentDbRepository()
        {
            DatabaseId = ConfigurationManager.AppSettings[DatabaseIdKeyName];
            Endpoint = ConfigurationManager.AppSettings[EndpointKeyName];
            AuthKey = ConfigurationManager.AppSettings[AuthKeyName];

            CollectionId = SetCollectionId();
            CollectionUri = UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId);

            Client = new DocumentClient(new Uri(Endpoint),
                AuthKey,
                new ConnectionPolicy
                {
                    ConnectionMode = ConnectionMode.Direct,
                    ConnectionProtocol = Protocol.Tcp
                });
        }

        public TColl Get(object id)
        {
            var doc = Client.CreateDocumentQuery(CollectionUri)
                .Where(x => x.Id == id).AsEnumerable().FirstOrDefault();

            return doc != null ? JsonConvert.DeserializeObject<TColl>(doc.ToString()) : null;
        }

        public IEnumerable<TColl> GetAll()
        {
            var result = new List<TColl>();
            var docQuery = Client.CreateDocumentQuery<TColl>(CollectionUri).AsDocumentQuery();

            while (docQuery.HasMoreResults)
            {
                var input = docQuery.ExecuteNextAsync().GetAwaiter().GetResult();

                if (input.Count != 0)
                {
                    result.AddRange(input.Select(dObj =>
                    {
                        return (TColl)dObj;
                    }));
                }
            }

            return result;
        }

        public async Task<IEnumerable<TColl>> GetAllAsync()
        {
            var result = new List<TColl>();
            var docQuery = Client.CreateDocumentQuery<TColl>(CollectionUri).AsDocumentQuery();

            while (docQuery.HasMoreResults)
            {
                var input = await docQuery.ExecuteNextAsync<TColl>();

                if (input.Count != 0)
                {
                    result.AddRange(input);
                }
            }

            return result;
        }

        public TColl SingleOrDefault(Expression<Func<TColl, bool>> predicate)
        {
            var docQuery = Client.CreateDocumentQuery<TColl>(CollectionUri)
                                                                        .Where(predicate)
                                                                        .AsDocumentQuery();

            var result = docQuery.ExecuteNextAsync().GetAwaiter().GetResult().FirstOrDefault();

            return (TColl)result;
        }

        public IEnumerable<TColl> Find(Expression<Func<TColl, bool>> predicate)
        {
            var result = new List<TColl>();
            var docQuery = Client.CreateDocumentQuery<TColl>(CollectionUri)
                                                                        .Where(predicate)
                                                                        .AsDocumentQuery();

            while (docQuery.HasMoreResults)
            {
                var input = docQuery.ExecuteNextAsync().GetAwaiter().GetResult();

                if (input.Count != 0)
                {
                    result.AddRange(input.Select(dObj =>
                    {
                        return (TColl)dObj;
                    }));
                }
            }

            return result;
        }

        public async Task<IEnumerable<TColl>> FindAsync(Expression<Func<TColl, bool>> predicate)
        {
            var result = new List<TColl>();
            var docQuery = Client.CreateDocumentQuery<TColl>(CollectionUri,
                new FeedOptions
                {
                    MaxItemCount = 1000,
                    MaxDegreeOfParallelism = -1
                })
                .Where(predicate)
                .AsDocumentQuery();

            while (docQuery.HasMoreResults)
            {
                var input = await docQuery.ExecuteNextAsync<TColl>().ConfigureAwait(false);

                if (input.Count() != 0)
                {
                    result.AddRange(input);
                }
            }

            return result;
        }

        public void Add(TColl entity)
        {
            Client.CreateDocumentAsync(CollectionUri, entity).GetAwaiter().GetResult();
        }

        public async Task AddAsync(TColl entity)
        {
            await Client.CreateDocumentAsync(CollectionUri, entity);
        }

        public void AddRange(IEnumerable<TColl> entities)
        {
            foreach (var e in entities)
            {
                Client.CreateDocumentAsync(CollectionUri, e).GetAwaiter().GetResult();
            }
        }

        public TColl Remove(object id)
        {
            var doc = Client.CreateDocumentQuery(CollectionUri)
                        .Where(x => x.Id == id).AsEnumerable().FirstOrDefault();

            if (doc == null)
            {
                return null;
            }

            var response = Client.DeleteDocumentAsync(doc.SelfLink).GetAwaiter().GetResult();

            return response.StatusCode == HttpStatusCode.NoContent
                ? JsonConvert.DeserializeObject<TColl>(doc.ToString())
                : default(TColl);
        }

        public async Task<TColl> RemoveAsync(object id)
        {
            var doc = Client.CreateDocumentQuery(CollectionUri)
                            .Where(x => x.Id == id).AsEnumerable().FirstOrDefault();

            if (doc == null)
            {
                return null;
            }

            var response = await Client.DeleteDocumentAsync(doc.SelfLink);

            return response.StatusCode == HttpStatusCode.NoContent
                ? JsonConvert.DeserializeObject<TColl>(doc.ToString())
                : default(TColl);
        }

        public IEnumerable<TColl> RemoveRange(IEnumerable<object> ids)
        {
            var deleted = new List<TColl>();

            foreach (var id in ids)
            {
                var doc = Client.CreateDocumentQuery(CollectionUri)
                            .Where(x => x.Id == id).AsEnumerable().FirstOrDefault();

                if (doc != null)
                {
                    var response = Client.DeleteDocumentAsync(doc.SelfLink).GetAwaiter().GetResult();

                    if (response.StatusCode == HttpStatusCode.NoContent)
                    {
                        deleted.Add(JsonConvert.DeserializeObject<TColl>(doc.ToString()));
                    }
                }
            }

            return deleted;
        }

        public async Task<IEnumerable<TColl>> RemoveRangeAsync(IEnumerable<object> ids)
        {
            var deleted = new List<TColl>();

            foreach (var id in ids)
            {
                var doc = Client.CreateDocumentQuery(CollectionUri)
                            .Where(x => x.Id == id).AsEnumerable().FirstOrDefault();

                if (doc != null)
                {
                    var response = await Client.DeleteDocumentAsync(doc.SelfLink);

                    if (response.StatusCode == HttpStatusCode.NoContent)
                    {
                        deleted.Add(JsonConvert.DeserializeObject<TColl>(doc.ToString()));
                    }
                }
            }

            return deleted;
        }

        public async Task RemoveAllAsync()
        {
            await Client.DeleteDocumentCollectionAsync(CollectionUri);
        }

        private string SetCollectionId()
        {
            var type = typeof(TColl).ToString();

            switch (type)
            {
                case "DomainModel.DTOs.Devices.DevicesData":
                    {
                        return ConfigurationManager.AppSettings[DeviceMeasurementsCollectionIdKey];
                    }
                default:
                    {
                        return string.Empty;
                    }
            }
        }
    }
}

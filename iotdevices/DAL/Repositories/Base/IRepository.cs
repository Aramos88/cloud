﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DAL.Repositories.Base
{
    public interface IRepository<TEntity>
        where TEntity : class
    {
        TEntity Get(object id);
        IEnumerable<TEntity> GetAll();
        Task<IEnumerable<TEntity>> GetAllAsync();

        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate);

        void Add(TEntity entity);
        Task AddAsync(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);

        TEntity Remove(object id);
        Task<TEntity> RemoveAsync(object id);
        IEnumerable<TEntity> RemoveRange(IEnumerable<object> entities);
        Task<IEnumerable<TEntity>> RemoveRangeAsync(IEnumerable<object> ids);
        Task RemoveAllAsync();
    }
}

﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DAL.Services
{
    public class EmailService : IIdentityMessageService
    {
        private readonly string SendGridApiKey = "sg:SendgridApiKey";
        private readonly string MailServiceApiKey;

        public EmailService()
        {
            MailServiceApiKey = ConfigurationManager.AppSettings[SendGridApiKey];
        }

        public async Task SendAsync(IdentityMessage message)
        {
            await ElaborateMessage(message);
        }

        private async Task ElaborateMessage(IdentityMessage message)
        {
            var sg = new SendGridAPIClient(MailServiceApiKey);

            var mail = new Mail(new Email("isaac_lara@sendgrid.com"),
                message.Subject,
                new Email(message.Destination),
                new Content("text/html", message.Body));

            dynamic response = await sg.client.mail.send.post(requestBody: mail.Get());
        }
    }
}

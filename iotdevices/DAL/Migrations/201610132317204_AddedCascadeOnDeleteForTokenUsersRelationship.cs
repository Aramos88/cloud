namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCascadeOnDeleteForTokenUsersRelationship : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tokens", "ApplicationUserId", "dbo.ApplicationUsers");
            AddForeignKey("dbo.Tokens", "ApplicationUserId", "dbo.ApplicationUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tokens", "ApplicationUserId", "dbo.ApplicationUsers");
            AddForeignKey("dbo.Tokens", "ApplicationUserId", "dbo.ApplicationUsers", "Id");
        }
    }
}

namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.C2DArduinoMessage",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplicationUserId = c.String(maxLength: 128),
                        ApplicationDeviceId = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Parameters = c.String(),
                        CreatedAt = c.DateTime(),
                        ResponseFeedBack = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUserId)
                .ForeignKey("dbo.ApplicationDevices", t => t.ApplicationDeviceId, cascadeDelete: true)
                .Index(t => t.ApplicationUserId)
                .Index(t => t.ApplicationDeviceId);
            
            CreateTable(
                "dbo.ApplicationDevices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        Code = c.String(nullable: false),
                        Key = c.String(nullable: false),
                        DeviceName = c.String(nullable: false, maxLength: 80),
                        LeasedOrSold = c.Boolean(nullable: false),
                        ModelNumber = c.String(nullable: false, maxLength: 100),
                        SerialNumber = c.String(nullable: false, maxLength: 100),
                        SimulationMode = c.Boolean(nullable: false),
                        CurrentState = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        CloudToDeviceMessageCount = c.Int(nullable: false),
                        ConnectionStateUpdatedTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastActivityTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        SensorTypeId = c.Int(nullable: false),
                        ApplicationUserId = c.String(nullable: false, maxLength: 128),
                        SubscriptionId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUserId, cascadeDelete: true)
                .ForeignKey("dbo.SensorTypes", t => t.SensorTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Subscriptions", t => t.SubscriptionId)
                .Index(t => t.SensorTypeId)
                .Index(t => t.ApplicationUserId)
                .Index(t => t.SubscriptionId);
            
            CreateTable(
                "dbo.ApplicationUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(nullable: false, maxLength: 30),
                        LastName = c.String(nullable: false, maxLength: 60),
                        JoinedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.UserDeviceRegistrations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsCompleted = c.Boolean(nullable: false),
                        ConfirmationToken = c.String(nullable: false),
                        CompletedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DeviceCodes = c.String(),
                        ApplicationUserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUserId, cascadeDelete: true)
                .Index(t => t.ApplicationUserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.RoleUsers",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.RoleId)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.Tokens",
                c => new
                    {
                        ApplicationUserId = c.String(nullable: false, maxLength: 128),
                        AuthToken = c.String(nullable: false, maxLength: 700),
                        IssuedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ExpiresOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ApplicationUserId)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUserId)
                .Index(t => t.ApplicationUserId);
            
            CreateTable(
                "dbo.DeviceMeasurements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SentAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ReceivedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        EventProcessedUtcTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        EventEnqueuedUtcTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Data = c.String(nullable: false),
                        ApplicationDeviceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationDevices", t => t.ApplicationDeviceId, cascadeDelete: true)
                .Index(t => t.ApplicationDeviceId);
            
            CreateTable(
                "dbo.SensorTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Subscriptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContactMail = c.String(nullable: false, maxLength: 100),
                        NumberOfDevices = c.Int(nullable: false),
                        PlanType = c.Int(nullable: false),
                        BeginningDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DueDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TotalDue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalPayed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Tax = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ServiceFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ContactInfo_Email = c.String(nullable: false, maxLength: 100),
                        ContactInfo_Address = c.String(nullable: false, maxLength: 200),
                        ContactInfo_HomePhone = c.String(),
                        ContactInfo_MobilePhone = c.String(),
                        SubscriptionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Subscriptions", t => t.SubscriptionId, cascadeDelete: true)
                .Index(t => t.SubscriptionId);
            
            CreateTable(
                "dbo.DeviceDiagnostics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BatteryLife = c.String(),
                        StatusConnection = c.String(),
                        Temperature = c.String(),
                        ApplicationDeviceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationDevices", t => t.ApplicationDeviceId, cascadeDelete: true)
                .Index(t => t.ApplicationDeviceId);
            
            CreateTable(
                "dbo.MessagesC2D",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserSending = c.String(maxLength: 100),
                        BodyMessage_DeviceIp = c.String(maxLength: 100),
                        BodyMessage_Msg = c.String(maxLength: 255),
                        DeviceName = c.String(maxLength: 100),
                        CreatedAt = c.DateTime(nullable: false),
                        ResponseFeedBack = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RoleUsers", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.DeviceDiagnostics", "ApplicationDeviceId", "dbo.ApplicationDevices");
            DropForeignKey("dbo.Invoices", "SubscriptionId", "dbo.Subscriptions");
            DropForeignKey("dbo.ApplicationDevices", "SubscriptionId", "dbo.Subscriptions");
            DropForeignKey("dbo.ApplicationDevices", "SensorTypeId", "dbo.SensorTypes");
            DropForeignKey("dbo.DeviceMeasurements", "ApplicationDeviceId", "dbo.ApplicationDevices");
            DropForeignKey("dbo.C2DArduinoMessage", "ApplicationDeviceId", "dbo.ApplicationDevices");
            DropForeignKey("dbo.ApplicationDevices", "ApplicationUserId", "dbo.ApplicationUsers");
            DropForeignKey("dbo.Tokens", "ApplicationUserId", "dbo.ApplicationUsers");
            DropForeignKey("dbo.RoleUsers", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.AspNetUserLogins", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.UserDeviceRegistrations", "ApplicationUserId", "dbo.ApplicationUsers");
            DropForeignKey("dbo.AspNetUserClaims", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.C2DArduinoMessage", "ApplicationUserId", "dbo.ApplicationUsers");
            DropIndex("dbo.Roles", "RoleNameIndex");
            DropIndex("dbo.DeviceDiagnostics", new[] { "ApplicationDeviceId" });
            DropIndex("dbo.Invoices", new[] { "SubscriptionId" });
            DropIndex("dbo.DeviceMeasurements", new[] { "ApplicationDeviceId" });
            DropIndex("dbo.Tokens", new[] { "ApplicationUserId" });
            DropIndex("dbo.RoleUsers", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.RoleUsers", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.UserDeviceRegistrations", new[] { "ApplicationUserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.ApplicationDevices", new[] { "SubscriptionId" });
            DropIndex("dbo.ApplicationDevices", new[] { "ApplicationUserId" });
            DropIndex("dbo.ApplicationDevices", new[] { "SensorTypeId" });
            DropIndex("dbo.C2DArduinoMessage", new[] { "ApplicationDeviceId" });
            DropIndex("dbo.C2DArduinoMessage", new[] { "ApplicationUserId" });
            DropTable("dbo.Roles");
            DropTable("dbo.MessagesC2D");
            DropTable("dbo.DeviceDiagnostics");
            DropTable("dbo.Invoices");
            DropTable("dbo.Subscriptions");
            DropTable("dbo.SensorTypes");
            DropTable("dbo.DeviceMeasurements");
            DropTable("dbo.Tokens");
            DropTable("dbo.RoleUsers");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.UserDeviceRegistrations");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.ApplicationUsers");
            DropTable("dbo.ApplicationDevices");
            DropTable("dbo.C2DArduinoMessage");
        }
    }
}

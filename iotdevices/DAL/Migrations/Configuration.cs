namespace DAL.Migrations
{
    using Context;
    using DomainModel.Entities.Business;
    using DomainModel.Entities.Identity;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = true;
            CommandTimeout = 600;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            SeedSensorTypes(context);

            SeedSuperAdminAndDevices(context);

            SeedRoles(context);
        }

        private void SeedRoles(ApplicationDbContext context)
        {
            using (var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context)))
            {
                using (var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context)))
                {
                    if (roleManager.Roles.Count() == 0)
                    {
                        roleManager.Create(new IdentityRole { Name = "SuperAdmin" });
                        roleManager.Create(new IdentityRole { Name = "Admin" });
                        roleManager.Create(new IdentityRole { Name = "User" });
                    }
                }

                manager.AddToRoles(manager.FindByName("SuperPowerUser").Id, new string[] { "SuperAdmin", "Admin" });
                context.SaveChanges();
            }
        }

        private void SeedSuperAdminAndDevices(ApplicationDbContext context)
        {
            using (var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context)))
            {
                var user = new ApplicationUser
                {
                    UserName = "SuperPowerUser",
                    Email = "ramos.anton88@gmail.com",
                    FirstName = "Antonio",
                    LastName = "Ramos",
                    JoinedOn = DateTime.Now,
                    EmailConfirmed = true,
                    Token = new Token
                    {
                        AuthToken = Guid.NewGuid().ToString(),
                        IssuedOn = DateTime.Now,
                        ExpiresOn = DateTime.Now.AddMinutes(Convert.ToDouble(15))
                    },
                    Devices = new List<ApplicationDevice>
                    {
                        new ApplicationDevice
                        {
                            DeviceName = "Temperature",
                            Key = "3muvpLz9eC3vStqCH9A2on/ILP6/ThyXmV/afuN6deg=",
                            Code = "Temperature_376681cc-8ee2-49ab-9526-ebaf9a31d607",
                            Description = "any description",
                            SerialNumber = Guid.NewGuid().ToString(),
                            ModelNumber = Guid.NewGuid().ToString(),
                            SensorTypeId = 1
                        },
                        new ApplicationDevice
                        {
                            DeviceName = "Ph",
                            Key = "Nn7BP/h16D4V8QKTneyw9E0p1HE+4/PLru0IahJwrr8=",
                            Code = "pH_e44982fc-5851-4b44-8b2a-0bbebe5dc3aa",
                            Description = "any description",
                            SerialNumber = Guid.NewGuid().ToString(),
                            ModelNumber = Guid.NewGuid().ToString(),
                            SensorTypeId = 2
                        },
                        new ApplicationDevice
                        {
                            DeviceName = "LightIntensity",
                            Key = "vNx8bl/TODzfuUdNRpjf3bowz8aOrQWBpH293fuznjA=",
                            Code = "Ambient_Light_Intensity_1b941be8-8fb9-4ff7-a21e-a50a836a2913",
                            Description = "any description",
                            SerialNumber = Guid.NewGuid().ToString(),
                            ModelNumber = Guid.NewGuid().ToString(),
                            SensorTypeId = 3
                        },
                        new ApplicationDevice
                        {
                            DeviceName = "Volume",
                            Key = "ktxKLvMQrRbi+yCQmFwQwNDsZqRfz3qFpNn5w/fK994=",
                            Code = "Volume_2de1ab20-4a4f-4a0d-9be8-f767cea547ac",
                            Description = "any description",
                            SerialNumber = Guid.NewGuid().ToString(),
                            ModelNumber = Guid.NewGuid().ToString(),
                            SensorTypeId = 4
                        },
                        new ApplicationDevice
                        {
                            DeviceName = "ElectricCurrent",
                            Key = "FVztijmRDakRyohtQ07PpelToR5z4MxTaKefrwmNkqc=",
                            Code = "Electric_Current_63a852b0-f9d1-4331-9ec7-ac1af30df2b6",
                            Description = "any description",
                            SerialNumber = Guid.NewGuid().ToString(),
                            ModelNumber = Guid.NewGuid().ToString(),
                            SensorTypeId = 5
                        },
                        new ApplicationDevice
                        {
                            DeviceName = "Color",
                            Key = "6qX+XYLwlfMeNG5aA1GYDY2w1OdamBsTp4HqKqSfhDI=",
                            Code = "Color_a606acdc-182c-47dd-b715-8062a15d11c7",
                            Description = "any description",
                            SerialNumber = Guid.NewGuid().ToString(),
                            ModelNumber = Guid.NewGuid().ToString(),
                            SensorTypeId = 6
                        },
                        new ApplicationDevice
                        {
                            DeviceName = "Distance",
                            Key = "mzFQgELvF8lPcqSwdL2d7WSIa74SpLphdv6srlCKEGs=",
                            Code = "Distance_16e9543a-28f2-426e-b793-9375192f3fd1",
                            Description = "any description",
                            SerialNumber = Guid.NewGuid().ToString(),
                            ModelNumber = Guid.NewGuid().ToString(),
                            SensorTypeId = 7
                        },
                        new ApplicationDevice
                        {
                            DeviceName = "Force",
                            Key = "qRrGBjULY/tAC3vf0UIuGCCCuHTGWrTHO2c3HRn2XQ4=",
                            Code = "Force_8c436739-355c-45f9-acbb-a1357ea782cb",
                            Description = "any description",
                            SerialNumber = Guid.NewGuid().ToString(),
                            ModelNumber = Guid.NewGuid().ToString(),
                            SensorTypeId = 8
                        },
                        new ApplicationDevice
                        {
                            DeviceName = "Humidity",
                            Key = "ve9O4own6Ii8l0V7IA0VHoB4UXf9nFxeGZAUJiT8GG0=",
                            Code = "Humidity_33a30ac1-b2ae-47e9-8157-cce91eb3f8c8",
                            Description = "any description",
                            SerialNumber = Guid.NewGuid().ToString(),
                            ModelNumber = Guid.NewGuid().ToString(),
                            SensorTypeId = 9
                        },
                        new ApplicationDevice
                        {
                            DeviceName = "XZDistance",
                            Key = "n/xMo9IeH5riG2nTilcv18BvukUl/q3ZPRHvWanJSFo=",
                            Code = "X-Z_Plane_Distance_e485840d-994a-4745-a40b-16ddf681a9f2",
                            Description = "any description",
                            SerialNumber = Guid.NewGuid().ToString(),
                            ModelNumber = Guid.NewGuid().ToString(),
                            SensorTypeId = 10
                        },
                        new ApplicationDevice
                        {
                            DeviceName = "Volumetric_Flow",
                            Key = "NVToVaA3M6HJ+/aOIzSVV5vFDEChmOos6bkDfzQ8aXQ=",
                            Code = "Volumetric_Flow_313c51d4-072d-46be-8ed3-f52181b1dcac",
                            Description = "any description",
                            SerialNumber = Guid.NewGuid().ToString(),
                            ModelNumber = Guid.NewGuid().ToString(),
                            SensorTypeId = 11
                        },
                        new ApplicationDevice
                        {
                            DeviceName = "Presence",
                            Key = "7k8aNfF/FhE9Dfp9jLlBZ0MMQAUrRU0zC9ifvrV2OHo=",
                            Code = "Presence_313c51d4-072d-46be-8ed3-f52181b1dcac",
                            Description = "any description",
                            SerialNumber = Guid.NewGuid().ToString(),
                            ModelNumber = Guid.NewGuid().ToString(),
                            SensorTypeId = 12
                        }
                    }
                };

                manager.Create(user, "MySuperP@ssword!");
                context.SaveChanges();
            }
        }

        private void SeedSensorTypes(ApplicationDbContext context)
        {
            if (context.SensorTypes.Count() < 13)
            {
                context.SensorTypes.AddRange(new List<SensorType>
                {
                    new SensorType
                    {
                        Value = "Temperature"
                    },
                    new SensorType
                    {
                        Value = "Ph"
                    },
                    new SensorType
                    {
                        Value = "Ambient Light Intensity"
                    },
                    new SensorType
                    {
                        Value = "Fluid Volume"
                    },
                    new SensorType
                    {
                        Value = "Electric Current"
                    },
                    new SensorType
                    {
                        Value = "Color"
                    },
                    new SensorType
                    {
                        Value = "Distance"
                    },
                    new SensorType
                    {
                        Value = "Force"
                    },
                    new SensorType
                    {
                        Value = "Humidity"
                    },
                    new SensorType
                    {
                        Value = "X-Z Plane Distance"
                    },
                    new SensorType
                    {
                        Value = "Volumetric Flow Rate"
                    },
                    new SensorType
                    {
                        Value = "Presence"
                    },
                    new SensorType
                    {
                        Value = "Gas"
                    }
                });
            }

            context.SaveChanges();
        }
    }
}

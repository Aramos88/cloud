﻿using Microsoft.AspNet.Identity.EntityFramework;
using DAL.Configurations;
using System.Data.Entity;
using DomainModel.Entities.Identity;
using DomainModel.Entities.Business;

namespace DAL.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        private const string ConnectionName = "DeviceConnectionString";

        public ApplicationDbContext()
            : base(ConnectionName)
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;

        }

        public ApplicationDbContext(string connectionName)
            : base(connectionName)
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Token> Tokens { get; set; }
        public DbSet<ApplicationDevice> Devices { get; set; }
        public DbSet<DeviceMeasurement> DeviceMeasurements { get; set; }
        public DbSet<SensorType> SensorTypes { get; set; }
        public DbSet<MessagesC2D> MessagesC2D { get; set; }
        public DbSet<C2DArduinoMessage> C2DArduinoMessages { get; set; }
        public DbSet<DeviceDiagnostic> DeviceDiagnostics { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<UserDeviceRegistration> UserDeviceRegistrations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>().ToTable("Users");
            modelBuilder.Entity<IdentityRole>().ToTable("Roles");
            modelBuilder.Entity<IdentityUserRole>().ToTable("RoleUsers");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaims");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogins");

            modelBuilder.Configurations.Add(new ApplicationUserConfiguration());
            modelBuilder.Configurations.Add(new TokenConfiguration());

            modelBuilder.Configurations.Add(new ApplicationDeviceConfiguration());
            modelBuilder.Configurations.Add(new ContactInfoConfiguration());
            modelBuilder.Configurations.Add(new UserDeviceRegistrationConfiguration());
            modelBuilder.Configurations.Add(new InvoiceConfiguration());
            modelBuilder.Configurations.Add(new SubscriptionConfiguration());
            modelBuilder.Configurations.Add(new DeviceDiagnosticsConfiguration());
            modelBuilder.Configurations.Add(new DeviceMeasurementConfiguration());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}

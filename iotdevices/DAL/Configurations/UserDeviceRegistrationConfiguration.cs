﻿using DomainModel.Entities.Business;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DAL.Configurations
{
    public class UserDeviceRegistrationConfiguration : EntityTypeConfiguration<UserDeviceRegistration>
    {
        public UserDeviceRegistrationConfiguration()
        {
            HasKey(udr => udr.Id);
            Property(udr => udr.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(udr => udr.ConfirmationToken).IsRequired();
            Property(udr => udr.CompletedOn).HasColumnType("datetime2");

            HasRequired(udr => udr.ApplicationUser)
                .WithMany(au => au.DeviceRegistrations)
                .HasForeignKey(udr => udr.ApplicationUserId)
                .WillCascadeOnDelete(true);
        }
    }
}

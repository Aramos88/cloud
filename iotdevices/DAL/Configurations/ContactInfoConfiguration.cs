﻿using DomainModel.Entities.Business;
using System.Data.Entity.ModelConfiguration;

namespace DAL.Configurations
{
    public class ContactInfoConfiguration : ComplexTypeConfiguration<ContactInfo>
    {
        public ContactInfoConfiguration()
        {
            Property(ci => ci.Email).HasMaxLength(100).IsRequired();
            Property(ci => ci.Address).HasMaxLength(200).IsRequired();
        }
    }
}

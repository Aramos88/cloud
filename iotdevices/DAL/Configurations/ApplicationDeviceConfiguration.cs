﻿using DomainModel.Entities.Business;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DAL.Configurations
{
    public class ApplicationDeviceConfiguration : EntityTypeConfiguration<ApplicationDevice>
    {
        public ApplicationDeviceConfiguration()
        {
            HasKey(ad => ad.Id);
            Property(ad => ad.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(ad => ad.ConnectionStateUpdatedTime).HasColumnType("datetime2");
            Property(ad => ad.LastActivityTime).HasColumnType("datetime2");

            Property(d => d.Description).IsRequired();
            Property(d => d.Code).IsRequired();
            Property(d => d.Key).IsRequired();
            Property(ad => ad.DeviceName).HasMaxLength(80).IsRequired();
            Property(d => d.ModelNumber).HasMaxLength(100).IsRequired();
            Property(d => d.SerialNumber).HasMaxLength(100).IsRequired();

            HasRequired(d => d.ApplicationUser)
                .WithMany(au => au.Devices)
                .HasForeignKey(c => c.ApplicationUserId);

            HasRequired(ad => ad.SensorType);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel.Entities.Business;

namespace SqlData.Configurations
{
    public class C2DArduinoMessageConfiguration: EntityTypeConfiguration<C2DArduinoMessage>
    {
        public C2DArduinoMessageConfiguration()
        {
            HasKey(msg => msg.Id);
            Property(msg => msg.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(msg => msg.ApplicationUserId).IsRequired();
            Property(msg => msg.Parameters).HasMaxLength(2000).IsRequired();
            Property(msg => msg.Name).IsRequired();

            HasRequired(msg => msg.ApplicationDevice)
                .WithMany(dev => dev.C2DArduinoMessages)
                .HasForeignKey(msg => msg.ApplicationDeviceId);

            HasRequired(msg => msg.ApplicationUser)
                .WithMany(user => user.C2DMessages)
                .HasForeignKey(msg => msg.ApplicationDeviceId);
        }
    }
}

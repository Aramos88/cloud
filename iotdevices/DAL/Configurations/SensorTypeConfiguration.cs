﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using DomainModel.Entities.Business;

namespace DAL.Configurations
{
    public class SensorTypeConfiguration : EntityTypeConfiguration<SensorType>
    {
        public SensorTypeConfiguration()
        {
            HasKey(st => st.Id);
            Property(st => st.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(st => st.Value)
                .HasMaxLength(30)
                .IsRequired();
        }
    }
}

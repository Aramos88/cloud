﻿using DomainModel.Entities.Business;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DAL.Configurations
{
    public class SubscriptionConfiguration : EntityTypeConfiguration<Subscription>
    {
        public SubscriptionConfiguration()
        {
            HasKey(s => s.Id);
            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(s => s.ContactMail).HasMaxLength(100).IsRequired();

            Property(s => s.BeginningDate).HasColumnType("datetime2");
            Property(s => s.DueDate).HasColumnType("datetime2");

            HasMany(s => s.Devices)
                .WithOptional(ad => ad.Subscription)
                .HasForeignKey(ad => ad.SubscriptionId);
        }
    }
}

﻿using DomainModel.Entities.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DAL.Configurations
{
    public class TokenConfiguration : EntityTypeConfiguration<Token>
    {
        public TokenConfiguration()
        {
            Property(t => t.AuthToken).IsRequired().HasMaxLength(700);
            Property(t => t.IssuedOn).IsRequired().HasColumnType("datetime2");
            Property(t => t.ExpiresOn).IsRequired().HasColumnType("datetime2");

            HasRequired(t => t.ApplicationUser)
                .WithOptional(au => au.Token)
                .WillCascadeOnDelete(true);
        }
    }
}

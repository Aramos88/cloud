﻿using DomainModel.Entities.Identity;
using System.Data.Entity.ModelConfiguration;

namespace DAL.Configurations
{
    public class ApplicationUserConfiguration : EntityTypeConfiguration<ApplicationUser>
    {
        public ApplicationUserConfiguration()
        {
            HasOptional(au => au.Token).WithRequired(t => t.ApplicationUser).WillCascadeOnDelete(true);

            Property(au => au.FirstName).IsRequired().HasMaxLength(30);
            Property(au => au.LastName).IsRequired().HasMaxLength(60);
            Property(au => au.JoinedOn).IsRequired().HasColumnType("datetime2");
        }
    }
}

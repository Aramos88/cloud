﻿using DomainModel.Entities.Business;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DAL.Configurations
{
    public class DeviceDiagnosticsConfiguration : EntityTypeConfiguration<DeviceDiagnostic>
    {
        public DeviceDiagnosticsConfiguration()
        {
            HasKey(dd => dd.Id);
            Property(dd => dd.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}

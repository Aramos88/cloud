﻿using DomainModel.Entities.Business;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DAL.Configurations
{
    public class DeviceMeasurementConfiguration : EntityTypeConfiguration<DeviceMeasurement>
    {
        public DeviceMeasurementConfiguration()
        {
            HasKey(dm => dm.Id);
            Property(dm => dm.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(dm => dm.ApplicationDevice)
                .WithMany(ad => ad.Measurements)
                .HasForeignKey(d => d.ApplicationDeviceId);

            Property(dm => dm.Data).IsRequired();
            Property(dm => dm.SentAt).HasColumnType("datetime2");
            Property(dm => dm.ReceivedAt).HasColumnType("datetime2");
            Property(dm => dm.EventEnqueuedUtcTime).HasColumnType("datetime2");
            Property(dm => dm.EventProcessedUtcTime).HasColumnType("datetime2");
        }
    }
}

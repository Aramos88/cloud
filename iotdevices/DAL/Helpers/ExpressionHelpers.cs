﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DAL.Helpers
{
    public static class ExpressionHelpers
    {
        private static Dictionary<string, string> ExpressionToSqlReplacements;

        static ExpressionHelpers()
        {
            ExpressionToSqlReplacements = new Dictionary<string, string>
            {
                { "AndAlso", "AND" },
                { "OrElse", "OR" },
                { "==", "=" },
                { "\"", "'" }
            };
        }

        public static string GetFirstParameterName<TColl>(this Expression<Func<TColl, bool>> predicate)
        {
            return predicate.Parameters[0].Name;
        }

        public static string TranslateLambdaToDocumentDbQuery<TColl>(this Expression<Func<TColl, bool>> predicate)
        {
            var expression = predicate.Body.ToString();

            foreach (var kvp in ExpressionToSqlReplacements)
            {
                expression = expression.Replace(kvp.Key, kvp.Value);
            }

            return expression.ToLowerInvariant();
        }
    }
}
